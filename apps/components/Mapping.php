 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mapping extends BC_Controller {


	public function index()
	{
		#modules_mapping();
        #all_access();
        $this->modul->render('View File', $data, FALSE);
	}

	public function location_map()
	{
		//$this->template->set_layout('content_only');
		$id = $_GET['loc'];
		$this->load->model('mapping_model');
		$data['rack'] = $this->mapping_model->data_location($id);
		//print_out($data['rack']);
        $this->modules->render('/mapping/location_map', $data);
	}

	public function rack_map()
	{
		$this->template->set_layout('content_only');
		$id = $_GET['rack'];
		$this->load->model('mapping_model');
		$data['carton'] = $this->mapping_model->data_carton($id);
        $this->modules->render('/mapping/rack_map', $data);
	}
}

/* End of file mapping.php */
/* Location: ./application/controllers/mapping.php */
