<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Account Component
 *
 *
 * @package	    Apps
 * @subpackage	Component
 * @category	Component
 * 
 * @version     1.1 Build 07.12.2014	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Account extends BC_Controller {

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('account_model');
	}

	public function index()
	{
		if(isset($_POST['update'])):
			$this->update($_POST);
		else:
			$data = $this->arrayCastRecursive($this->ezrbac->getCurrentUser());
			$this->modules->render('/account/index',$data);
		endif;
	}

	private function arrayCastRecursive($array)
	{
	    if (is_array($array)) {
	        foreach ($array as $key => $value) {
	            if (is_array($value)) {
	                $array[$key] = $this->arrayCastRecursive($value);
	            }
	            if ($value instanceof stdClass) {
	                $array[$key] = $this->arrayCastRecursive((array)$value);
	            }
	        }
	    }
	    if ($array instanceof stdClass) {
	        return $this->arrayCastRecursive((array)$array);
	    }
	    return $array;
	}

	private function update($data)
	{
		$this->account_model->update($data);
		$output = array('errorcode' => 0, 'msg' => 'success');
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file account.php */
/* Location: ./apps/component/account.php */