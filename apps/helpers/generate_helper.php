<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Hospital Information System
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Generate Helpers
 *
 *
 * @package	    Helpers
 * @subpackage	
 * @category	Helpers
 * 
 * @version     1.0 Build 20.08.2014	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Get nomor Account
 *
 *
 * @access	public
 * @return	integer
 */
if ( ! function_exists('getAccountNo'))
{
	function getAccountNo($lvl,$sub,$dept=false)
	{
		if(function_exists('get_instance'));
        {
            $ci =& get_instance();
        }
        if($dept == 1){
	        $ci->db->where(T_MasterDataAccount_Level,$lvl);
	        $ci->db->where(T_MasterDataAccount_MasterSub,$sub);
	        $query = $ci->db->get(T_MasterDataAccount);
	    	$count = $query->num_rows()+1;
			$digit  = 4;
			$num = str_pad($count,$digit,0,STR_PAD_RIGHT);
			$result = $num;
	    }else{
	        $ci->db->where(T_MasterDataAccount_Depth,$dept);
	        $query = $ci->db->get(T_MasterDataAccount);
	    	$count = $query->num_rows()+1;
			
			if($lvl == 5 && $sub == 0){
				$def = 0000;
				$digit  = 4;
				$def = $def+$count;
				$num = str_pad($def,$digit,0,STR_PAD_LEFT);
				$num = $dept.".".$num;
				$result = $num;
			}else{
				$digit  = 4;
				$dept = substr($dept, 0,$lvl);
				$dept = $dept + $count;
				$num = str_pad($dept,$digit,0,STR_PAD_RIGHT);
				$result = $num;
			}
	    }
		return $result;
	}
}

/**
 * Get nomor pendaftaran
 *
 *
 * @access	public
 * @return	integer
 */
if ( ! function_exists('get_noPendaftaran'))
{
	function get_noPendaftaran($id)
	{
		if(function_exists('get_instance'));
        {
            $ci =& get_instance();
        }
        $ci->db->where('instalasi_id',$id);
		$query = $ci->db->get('admisi');
    	$data = $query->result();
    	$count = count($data)+1;
		$digit  = 3;
		if($id==10){
			$key = 'RJ';
		}elseif($id==11){
			$key = "RD";
		}elseif($id==12){
			$key = "RI";
		}else{
			$key = "PM";
		}
		$date = date('my');
		$num = str_pad($count,$digit,0,STR_PAD_LEFT);
		$result = $key.$date.$num;
		return $result;
	}
}

// ------------------------------------------------------------------------

/**
 * Get nomor pendaftaran
 *
 *
 * @access	public
 * @return	integer
 */
if ( ! function_exists('no_antrian'))
{
	function no_antrian($id)
	{
		if(function_exists('get_instance'));
        {
            $ci =& get_instance();
        }
        $ci->db->where('status_Id',ANTRI);
        $ci->db->where('instalasi_id',$id);
		$query = $ci->db->get('admisi');
    	$data = $query->result();
    	$count = count($data)+1;
		$digit  = 3;
		$num = str_pad($count,$digit,0,STR_PAD_LEFT);
		$result = $num;
		return $result;
	}
}

// ------------------------------------------------------------------------

/**
 * Get nomor reseptur
 *
 *
 * @access	public
 * @return	integer
 */
if ( ! function_exists('get_noResepture'))
{
	function get_noResepture()
	{
		if(function_exists('get_instance'));
        {
            $ci =& get_instance();
        }
		$query = $ci->db->get('lembar_resep');
    	$data = $query->result();
    	$count = count($data)+1;
		$digit  = 3;
		$key = 'RS';
		$date = date('my');
		$num = str_pad($count,$digit,0,STR_PAD_LEFT);
		$result = $key.$date.$num;
		return $result;
	}
}

// ------------------------------------------------------------------------

/**
 * Get nomor Mutasi
 *
 *
 * @access	public
 * @return	integer
 */
if ( ! function_exists('get_noMutasi'))
{
	function get_noMutasi()
	{
		if(function_exists('get_instance'));
        {
            $ci =& get_instance();
        }
		$query = $ci->db->get('mutasi');
    	$data = $query->num_rows();
    	$count = $data + 1;
		$digit  = 3;
		$key = 'MT';
		$date = date('my');
		$num = str_pad($count,$digit,0,STR_PAD_LEFT);
		$result = $key.$date.$num;
		return $result;
	}
}

// ------------------------------------------------------------------------

/**
 * generate qrcode
 *
 *
 * @access	public
 * @return	integer
 */
if ( ! function_exists('generate_qrcode'))
{
	function generate_qrcode()
	{
		if(function_exists('get_instance'));
        {
            $ci =& get_instance();
        }
		$ci->load->library('ciqrcode');

        $params['data'] = 'This is a text to encode become QR Code';
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = FCPATH.'qrcode.png';
        $qrcode = $ci->ciqrcode->generate($params);
    	echo $qrcode;
	}
}

/* End of file generate_helper.php */
/* Location: ./site/helpers/generate_helper.php */
