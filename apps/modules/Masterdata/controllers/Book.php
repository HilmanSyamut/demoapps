<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Fajar Rahmat Indrawan
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Modules
 *
 * Book Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Fajar Rahmat Indrawan
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Book extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Book_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/Book/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Book_model->getDetail($id);
		$this->modules->render('/Book/form', $data);
	}

	public function formPrint($id='')
	{
		$this->template->set_layout('content_only');
		$data = $this->Beginbalance_model->getDetail($id);
		$this->modules->render('/Book/formPrint', $data);
	}
	
	public function formDetail($id='')
	{
		$data = $this->Book_model->getDetail($id);
		$this->modules->render('/book/formDetail', $data);
	}

	public function getList()
    {
        $info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_MasterDataBook;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		
		$Customfilter = $this->input->get('customfilter');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'ASC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Book_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField, $Customfilter);

		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			if ($this->input->get('filter')) {
				$info->count = $getList->num_rows();
			}else{
				$info->count = $this->Book_model->getListCount($table,$Customfilter);
			}
		}else{
			$info->data = null;
			$info->errorcode = 101;
			$info->msg = "Data not found!";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
    }

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_MasterDataBook_BookID   => $this->input->post("BookID"),
					T_MasterDataBook_BookTitle   => $this->input->post("BookTitle"),
					T_MasterDataBook_BookISBN   => $this->input->post("BookISBN"),
					T_MasterDataBook_BookKeywords   => $this->input->post("BookKeywords"),
					T_MasterDataBook_BookSN   => $this->input->post("BookSN"),
					T_MasterDataBook_BookBarcode   => $this->input->post("BookBarcode"),
					T_MasterDataBook_BookYearpublished   => $this->input->post("BookYearpublished"),
					T_MasterDataBook_BookCategories   => $this->input->post("BookCategories"),
					T_MasterDataBook_GroupAuthorID   => $this->input->post("GroupAuthorID"),
					T_MasterDataBook_GroupPublisher   => $this->input->post("GroupPublisher"),
					T_MasterDataBook_BookPicture   => $this->input->post("BookPicture")				
				);
				$this->Book_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_MasterDataBook_RecordTimestamp, 'TimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_MasterDataBook_RecordID   => $this->input->post("RecordID"),
					T_MasterDataBook_RecordTimestamp   => $this->input->post("TimeStamp"),
                    T_MasterDataBook_RecordStatus => 0,
					T_MasterDataBook_BookID   => $this->input->post("BookID"),
					T_MasterDataBook_BookTitle   => $this->input->post("BookTitle"),
					T_MasterDataBook_BookISBN   => $this->input->post("BookISBN"),
					T_MasterDataBook_BookKeywords   => $this->input->post("BookKeywords"),
					T_MasterDataBook_BookSN   => $this->input->post("BookSN"),
					T_MasterDataBook_BookBarcode   => $this->input->post("BookBarcode"),
					T_MasterDataBook_BookYearpublished   => $this->input->post("BookYearpublished"),
					T_MasterDataBook_BookCategories   => $this->input->post("BookCategories"),
					T_MasterDataBook_GroupAuthorID   => $this->input->post("GroupAuthorID"),
					T_MasterDataBook_GroupPublisher   => $this->input->post("GroupPublisher"),
					T_MasterDataBook_BookPicture   => !empty($this->input->post("BookPicture")) ? $this->input->post("BookPicture") : $this->input->post("BookPictureExist")
				);

				$this->Book_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Book_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}
/* End of file Book.php */
/* Location: ./app/modules/MasterData/controllers/Book.php */