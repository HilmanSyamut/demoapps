<div id="grid"></div>

<script type="text/javascript">
        $(document).ready(function(){
        //Grid
            // var crudServiceBaseUrl = "https://demos.telerik.com/kendo-ui/service",
                var dataSource = new kendo.data.DataSource({
                    transport: {
                            read: {
                                type:"GET",
                                url: "http://www.muhammad-arief.com/service/getData.php",
                                dataType: "json"
                                // data: { page: "1", pageSize: "10"}
                            },
                            update: {
                                type:"POST",
                                url: "http://www.muhammad-arief.com/service/updateData.php",
                                dataType: "json"
                            },
                            destroy: {
                                type:"POST",
                                url: "http://www.muhammad-arief.com/service/deleteData.php",
                                dataType: "json"
                            },
                            create: {
                                type:"POST",
                                url: "http://www.muhammad-arief.com/service/insertData.php",
                                dataType: "json"
                            },

                            parameterMap: function(options, operation) {
                                console.log(options, '//method yang dipakai:', operation);

                                switch (operation)
                                {
                                    case "read":
                                        return options;
                                        break; 
                                   case "create": {
                                        return {
                                            nik : options.models[0].nik,
                                            first_name : options.models[0].first_name,
                                            last_name : options.models[0].last_name,
                                            tempat_lahir : options.models[0].tempat_lahir,
                                            tanggal_lahir : options.models[0].tanggal_lahir, 
                                            divisi : options.models[0].divisi,
                                            jabatan : options.models[0].jabatan
                                        }
                                        break;
                                    }
                                    case "update": {
                                        return {
                                            id : options.models[0].id,
                                            nik : options.models[0].nik,
                                            first_name : options.models[0].first_name,
                                            last_name : options.models[0].last_name,
                                            tempat_lahir : options.models[0].tempat_lahir,
                                            tanggal_lahir : options.models[0].tanggal_lahir, 
                                            divisi : options.models[0].divisi,
                                            jabatan : options.models[0].jabatan
                                        }
                                        break;
                                    }
                                    var model = options.models;
                                    for (var i = model.length - 1; i >= 0; i--) {
                                        return model[i];
                                    }

                                    case "destroy":
                                        return {id : options.models[0].id }
                                        break;
                                }

                            }
                        },
                        // filterable: true,
                        // sortable: true,
                        batch: true,
                        page:1,
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        sync: function(e) {
                            // $('#grid').data('kendoGrid').read();
                            // $('#grid').data('kendoGrid').refresh();
                        },

                        schema: {
                            data: function(response){

                                var data = response.data;
                                for(var i in data) {
                                    for (var key in data[i]) {
                                        if (key.match(/\d+/)) delete data[i][key];
                                    }
                                }
                                return data;
                            },
                            total: function(response){
                                console.log('total data', response.count);
                                return response.count;
                            },
                            model: {
                                id: "id",
                                fields: {
                                    // id: { 
                                    //     validation: { 
                                    //         required: true 
                                    //     } 
                                    // },
                                    nik: { 
                                        validation: { 
                                            required: true 
                                        } 
                                    },
                                    first_name: { 
                                        validation: { 
                                            required: true
                                        } 
                                    },
                                    last_name: { 
                                        validation: { 
                                            required: true
                                        } 
                                    },
                                    tempat_lahir: { 
                                        validation: { 
                                            required: true
                                        } 
                                    },
                                    tanggal_lahir: { 
                                        validation: { 
                                            required: true
                                        } 
                                    },
                                    divisi: { 
                                        validation: { 
                                            required: true
                                        } 
                                    },
                                    jabatan: { 
                                        validation: { 
                                            required: true
                                        } 
                                    }
                                }
                            }
                        }   
                    });

            $("#grid").kendoGrid({
                height: "550px",width: "100%",
                toolbar: ["create", "save", "cancel"],
                // toolbar: kendo.template('<a class="k-button k-button-icontext k-grid-add" href="<?php echo site_url("Masterdata/Employee/form"); ?>"><span class="k-icon k-add"></span>Add new</a>'),
                columns: [
                {
                    "title":"ID",
                    "width":"20px",
                    "field":"id",
                },
                {
                    "title":"NIK",
                    "width":"50px",
                    "field":"nik",
                },
                {
                    "title":"First Name",
                    "width":"50px",
                    "field":"first_name",
                },
                {
                    "title":"Last Name",
                    "width":"50px",
                    "field":"last_name",
                },
                {
                    "title":"Tempat Lahir",
                    "width":"50px",
                    "field":"tempat_lahir",
                },
                {
                    "title":"Tanggal Lahir",
                    "width":"50px",
                    "field":"tanggal_lahir",
                },
                {
                    "title":"Divisi",
                    "width":"50px",
                    "field":"divisi",
                },
                {
                    "title":"Jabatan",
                    "width":"50px",
                    "field":"jabatan",
                },
                {
                    command: "destroy",
                    title: " ",
                    width: 150,
                }
                ], 
                dataSource: dataSource,
                editable: true, 
                // autoGenerateColumns: false,
                // autoBind: true,     
                sortable: true,
                // resizable: true,
                // selectable: true,
                // scrollable: true,
                // reorderable:true,
                pageable: {
                    refresh: true,
                    pageSizes: true
                },
                navigatable: true           
            });
        });

    </script>