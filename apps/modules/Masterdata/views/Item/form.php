<style type="text/css">.mrg{margin-bottom:20px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_MasterDataItem_RecordID}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataItem_RecordID}) ? ${T_MasterDataItem_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_MasterDataItem_RecordTimestamp}) ? ${T_MasterDataItem_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Item ID</label>
                            <div class="col-md-6">
                                <?php $items=array( 
                                    'id'=> 'ItemID', 
                                    'class' => 'k-input k-textbox', 
                                    'style' => 'text-transform: uppercase;',
                                    'value' => isset(${T_MasterDataItem_ItemID}) ? ${T_MasterDataItem_ItemID} : "",
                                    'readonly' => "true"
                                ); 
                                if(!isset(${T_MasterDataItem_ItemID})){ unset($items['readonly']); }
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Item Name</label>
                            <div class="col-md-6">
                                <?php $items=array(
                                    'id'=> 'ItemName',
                                    'class' => 'k-input k-textbox',
                                    'style' => 'width:250px;',
                                    'value' => isset(${T_MasterDataItem_ItemName}) ? ${T_MasterDataItem_ItemName} : "",
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">UOM ID</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'UOMID', 
                                    'class'=>'k-input', 
                                    'value' => isset(${T_MasterDataItem_UOMID}) ? ${T_MasterDataItem_UOMID} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                                </div>
                        </div><br>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Auto ID Type</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'AutoIDType', 
                                    'class' => 'k-input', 
                                    'value' => isset(${T_MasterDataItem_AutoIDType}) ? ${T_MasterDataItem_AutoIDType} : "",  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Item Group</label>
                            <div class="col-md-9">
                                <input id="ItemGroupID" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataItem_GroupID}) ? ${T_MasterDataItem_GroupID} : ''; ?>" />
                                <?php $items=array(
                                    'id'=> 'ItemGroupName', 
                                    'class' => 'k-input k-textbox', 
                                    'value' => isset(${T_MasterDataItemGroup_Name}) ? ${T_MasterDataItemGroup_Name} : "",
                                    'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($items); echo"&nbsp;"; ?>
                                <div class="k-button" id="LookupEventGroupID"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Picture</label>
                            <input type="hidden" id="photo" value="<?php echo isset($meta{T_MasterDataItem_Picture}) ? $meta{T_MasterDataItem_Picture} : ''; ?>">
                            <?php
                                if(isset(${T_MasterDataItem_Picture})){
                                    $voImg = (${T_MasterDataItem_Picture} == '') ? "assets/backend/images/no-photo.png" : ${T_MasterDataItem_Picture};
                                }else{
                                    $voImg = "assets/upload/photo/no-photo.png";
                                }
                            ?>
                            <div class="col-md-6">
                                <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                                    <div id="preview" style="padding-left: 10px;padding-top: 10px;background-color:#eee;border-radius: 5%;width:220px;height:220px;">
                                        <img class="img-responsive" src="<?php echo $voImg; ?>" style="width:200px;height:200px;margin-bottom:10px;border-radius:5%;">
                                    </div>
                                </a>
                                <form id="form-photo" action="<?php echo site_url('en/System/User/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                                    <input id="uploadImage" type="file" accept="image/*" name="image" value="<?php echo isset(${T_MasterDataItem_Picture}) ? ${T_MasterDataItem_Picture} : ''; ?>" />
                                    <input name="ItemIDS" id="ItemIDS" value="" type="hidden"/> 
                                    <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                                    <input type="hidden" id="PictureExist" value="<?php echo isset(${T_MasterDataItem_Picture}) ? ${T_MasterDataItem_Picture} : ''; ?>" />
                                </form>
                                <div id="err"></div>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Barcode</label>
                            <div class="col-md-9">
                                <?php $items=array(
                                    'id'=> 'Barcode', 
                                    'class' => 'k-input k-textbox', 
                                    'style' => 'width:200px;',
                                    'value' => isset(${T_MasterDataItem_Barcode}) ? ${T_MasterDataItem_Barcode} : ""
                                ); 
                                echo form_input($items); echo"&nbsp;"; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Size</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'Size', 
                                    'class' => 'k-input',
                                    'value' => isset(${T_MasterDataItem_Size}) ? ${T_MasterDataItem_Size} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">EPC</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id'=> 'EPC',
                                    'class' => 'k-input k-textbox', 
                                    'style' => 'text-transform: uppercase;',
                                    'style' => 'width:200px;',  
                                    'value' => isset(${T_MasterDataItem_EPC}) ? ${T_MasterDataItem_EPC} : "",  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Color</label>
                            <div class="col-md-4">
                                <input id="picker">
                                <?php $items=array( 
                                    'id'=> 'Color', 
                                    'class' => 'k-input', 
                                    'value' => isset(${T_MasterDataItem_Color}) ? ${T_MasterDataItem_Color} : "",
                                ); 
                                echo form_input($items); ?>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Unit Price</label>
                            <div class="col-md-4">
                                <?php $items=array( 
                                    'id' => 'UnitPrice', 
                                    'class' => 'k-input',
                                    'value' => isset(${T_MasterDataItem_UnitPrice}) ? ${T_MasterDataItem_UnitPrice} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($items); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                        </div>
                    </div>
                </div>
            </div>
            <footer class="panel-footer">
                <a href="javascript:void(0)" onclick="<?php if(isset(${T_MasterDataItem_RecordID})){echo"update()";}else{echo"insert()";};?>" class="btn btn-primary" >Save</a>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
    </div>
</div>
<?php
//Person Type Lookup
    //field in database data to load
    $dataItemGroup = array(
        array('field' => T_MasterDataItemGroup_ID, 'title' => 'ID', 'width' => '100px'),
        array('field' => T_MasterDataItemGroup_Name, 'title' => 'Name', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnItemGroup = array(
        array('id' => 'ItemGroupID', 'column' => T_MasterDataItemGroup_ID),
        array('id' => 'ItemGroupName', 'column' => T_MasterDataItemGroup_Name),
    );

    $customfilter = array("");

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("GroupID", "Item Group", "500px", "Webservice/Read/Getlist", $dataItemGroup, $columnItemGroup,T_MasterDataItemGroup,"",$customfilter);
    
//Person Type Lookup
    //field in database data to load
    $dataIdentity = array(
        array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Key', 'width' => '100px'),
        array('field' => T_MasterDataGeneralTableValue_Description, 'title' => 'Description', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnIdentity = array(
        array('id' => 'IdentityTypeID', 'column' => T_MasterDataGeneralTableValue_RecordID),
        array('id' => 'IdentityTypeName', 'column' => T_MasterDataGeneralTableValue_Description),
    );

    $customfilter2 = array(
        T_MasterDataGeneralTableValue_PRI => "5",
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("IdentityID", "Identity", "500px", "Webservice/Read/Getlist", $dataIdentity, $columnIdentity,T_MasterDataGeneralTableValue,"",$customfilter2);

?>

<?php
if (isset($t8040r001)) {
    $ID = $t8040r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_MasterDataItem_RecordID}) ? ${T_MasterDataItem_RecordID} : 0; ?>;
$(document).ready(function() {
    $("#picker").kendoColorPicker({
        value: "<?php echo isset(${T_MasterDataItem_ColorCode}) ? ${T_MasterDataItem_ColorCode} : "#ffffff" ?>",
        buttons: false
    });
    $("#UnitPrice").kendoNumericTextBox().val();
    $("#UOMID").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select UOM",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 1},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#AutoIDType").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_RecordID; ?>",
        optionLabel: "Select AutoID Type",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 2},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Size").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Size",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 4},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
    $("#Color").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Color",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 5},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
});

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}
function insert()
    {
        var voData = {
            ItemID: $('#ItemID').val(),
            ItemName: $('#ItemName').val(),
            UOMID: $('#UOMID').val(),
            AutoIDType: $('#AutoIDType').val(),
            EPC: $('#EPC').val(),
            Barcode: $('#Barcode').val(),
            UnitPrice: $('#UnitPrice').val(),
            Picture: $('#photo').val(),
            GroupID: $('#ItemGroupID').val(),
            Color: $('#Color').val(),
            ColorCode: $('#picker').val(),
            Size: $('#Size').val()
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Item/insert'),
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var voData = {
        RecordID: $('#RecordID').val(),
        TimeStamp: $('#TimeStamp').val(),
        ItemID: $('#ItemID').val(),
        ItemName: $('#ItemName').val(),
        UOMID: $('#UOMID').val(),
        AutoIDType: $('#AutoIDType').val(),
        EPC: $('#EPC').val(),
        Barcode: $('#Barcode').val(),
        UnitPrice: $('#UnitPrice').val(),
        Picture: $('#photo').val(),
        GroupID: $('#ItemGroupID').val(),
        Color: $('#Color').val(),
        ColorCode: $('#picker').val(),
        Size: $('#Size').val(),
        PictureExist: $("#PictureExist").val()
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Masterdata/Item/update'); ?>",
               success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(site_url('Masterdata/Item/'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.ItemID == "") { valid = 0; msg += "Item ID is required" + "\r\n"; }
    if (voData.ItemName == "") { valid = 0; msg += "Item Name is required" + "\r\n"; }
    if (voData.UOMID == "") { valid = 0; msg += "UOMID is required" + "\r\n"; }
    if (voData.AutoIDType == "") { valid = 0; msg += "Auto id Type is required" + "\r\n"; }
    if (voData.EPC == "") { valid = 0; msg += "EPC is required" + "\r\n"; }
    if (voData.Barcode == "") { valid = 0; msg += "Barcode is required" + "\r\n"; }
    if (voData.UnitPrice == "") { valid = 0; msg += "UnitPrice is required" + "\r\n"; }
    if (voData.GroupID == "") { valid = 0; msg += "Group is required" + "\r\n"; }
    if (voData.Color == "") { valid = 0; msg += "Color Code is required" + "\r\n"; }
    if (voData.Size == "") { valid = 0; msg += "Size is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);

    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}

</script>