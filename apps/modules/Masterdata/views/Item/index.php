<style type="text/css">
    .customer-photo {
        display: inline-block;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background-size: 40px 38px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataItem_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataItem_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataItem_Picture  => array(0,1,'35px','Photo',1,'picture',1),
    T_MasterDataItem_ItemID  => array(1,1,'50px','Item ID',1,'string',1),
    T_MasterDataItem_ItemName  => array(1,1,'100px','Item Name',0,'string',0),
    T_MasterDataItem_UOMID => array(1,1,'50px','UOM ID',0,'string',0),
    T_MasterDataGeneralTableValue_Key => array(1,1,'50px','Auto ID Type',0,'string',0),
    T_MasterDataItem_EPC => array(1,1,'100px','EPC',0,'string',0),
    T_MasterDataItem_Barcode => array(1,1,'100px','Barcode',0,'string',0),
    T_MasterDataItem_UnitPrice => array(1,1,'80px','Unit Price',0,'string',0)
);
// Column DropdownList => |Text|URL|KEY|
$dropdownlist = array(
    T_MasterDataItem_UOMID => array('UOMID','Masterdata/Generaltable/getItemType/1',T_MasterDataGeneralTableValue_Key),
    T_MasterDataGeneralTableValue_RecordID => array('AutoIDType','Masterdata/Generaltable/getItemBiz/2',T_MasterDataGeneralTableValue_Key)

);
// variable attribute for gridview

$attr = array(
    'id'=>'grid',
    'actBTN' => "75px",
    'postBTN' => "0",
    'table' => T_MasterDataItem,
    'tools' => array(
        T_MasterDataItem_RecordID,
        T_MasterDataItem_RecordTimestamp,
        T_MasterDataItem_ItemID,
        T_MasterDataItem_ItemName,
        T_MasterDataItem_UOMID,
        T_MasterDataGeneralTableValue_Key,
        T_MasterDataItem_EPC,
        T_MasterDataItem_Barcode,
        T_MasterDataItem_UnitPrice),
    'column' => $column,
    'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Masterdata/Item/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Item/Update',
        'destroy' => 'Masterdata/Item/Delete',
        'form' => 'Masterdata/Item/Form',
        'post' => '',
        'unpost' => ''
    )
);
// generate gridView
echo onlygridview($attr); 
?>