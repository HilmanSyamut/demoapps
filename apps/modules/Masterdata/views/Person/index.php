<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataPerson_RecordID  => array(0,0,'10px','Record ID',1,'string',1),
    T_MasterDataPerson_RecordTimestamp  => array(0,0,'10px','Record TimeStamp',1,'string',1),
    T_MasterDataPerson_PersonID  => array(1,1,'100px','Person ID',1,'string',1),
    T_MasterDataPerson_PersonName  => array(1,1,'100px','Person Name',0,'string',0)
    // T_MasterDataperson_RecordStatus  => array(1,1,'10px','Employee Status',0,'string',0),
    // T_MasterDataperson_RecordFlag  => array(1,1,'10px','Employee Flag',0,'string',0),
    // T_MasterDataperson_UpdatedOn  => array(1,1,'10px','Employee Update On',0,'string',0),
    // T_MasterDataperson_UpdatedBy  => array(1,1,'10px','Employee Update By',0,'string',0),
    // T_MasterDataperson_Address  => array(1,1,'10px','Employee Address',0,'string',0),
    // T_MasterDataperson_Country  => array(1,1,'10px','Employee Address',0,'string',0)
);
// Column DropdownList => |Text|URL|a
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'person',
    'table' => T_MasterDataPerson,
    'tools' => array(T_MasterDataPerson_RecordID,T_MasterDataPerson_RecordTimestamp,T_MasterDataPerson_PersonID,T_MasterDataPerson_PersonName),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'Masterdata/Person/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Person/Update',
        'destroy' => 'Masterdata/Person/Delete'
    )
);
// generate gridView
echo simpleGridView($attr); 
?>