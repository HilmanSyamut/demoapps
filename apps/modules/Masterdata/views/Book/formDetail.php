<div class="row">
    <div class="col-md-12">
        <?php echo form_open( '',array( 'id'=>'form-insert', 'class'=>'form-horizontal')); ?>
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title">Detail</h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordID}) ? ${T_TransactionStockMovementHeader_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_TransactionStockMovementHeader_RecordTimestamp}) ? ${T_TransactionStockMovementHeader_RecordTimestamp} : ''; ?>">

                <div class="tabs responsive tabs-primary">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#Detail" data-toggle="tab">Detail</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="Detail" class="tab-pane active">
                            <div style="overflow:auto;">
                            <input id="DoRemoveID" type="hidden" />
                                <table id="table-detail" class="table table-responsive">
                                    <thead id="head-detail">
                                        <tr>
                                            <th width="80px">Action</th>
                                            <th data-col="RowIndex">#</th>
                                            <th data-col="ItemID">Book ID</th>
                                            <th data-col="ItemName">Book Name</th>
                                            <th data-col="ItemType">Category</th>
                                            <th data-col="Qty">Penulis</th>
                                            <th data-col="LocationID">Penerbit</th>
                                            <th data-col="LocationName">Penulis</th>
                                            <th data-col="EPC">SN</th>
                                            <th data-col="Barcode">Barcode</th>
                                            <th data-col="RemarksDetail">Tahun Diterbikan</th>
                                            <th data-col="RecordIDDetail" style="display:none;"></th>
                                            <th data-col="RecordFlag" style="display:none;"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="list-detail">
                                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                                        foreach($Detail as $item): $detail .= '<tr id="detail-'.$i. '">
                                        <td class="actions"><a onclick="editdetail('.$target. ','.$i. ',0);" href="javascript:void(0);"><i class="fa fa-search"></i></a></td>
                                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                                        <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_ItemID]. '">'.$item[T_TransactionStockMovementDetail_ItemID]. '</td>
                                        <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_MasterDataBook_BookTitle]. '">'.$item[T_MasterDataBook_BookTitle]. '</td>
                                        <td id="detailItemTypev-'.$i. '" data-val="'.$item[T_MasterDataBook_BookCategories]. '">'.$item[T_MasterDataBook_BookCategories]. '</td>
                                        <td id="detailQtyv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Quantity1]. '">'.$item[T_TransactionStockMovementDetail_Quantity1]. '</td>
                                        <td id="detailLocationIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_LocationID1]. '">'.$item[T_TransactionStockMovementDetail_LocationID1]. '</td>
                                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_MasterDataLocation_LocationName]. '">'.$item[T_MasterDataLocation_LocationName]. '</td>
                                        <td id="detailEPCv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_EPC]. '">'.$item[T_TransactionStockMovementDetail_EPC]. '</td>
                                        <td id="detailBarcodev-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Barcode]. '">'.$item[T_TransactionStockMovementDetail_Barcode]. '</td>
                                        <td id="detailRemarksDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Remarks]. '">'.$item[T_TransactionStockMovementDetail_Remarks]. '</td>
                                        <td id="detailRecordIDDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordID]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordID]. '</td>
                                        <td id="detailRecordFlagv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordFlag]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordFlag]. '</td>
                                    </tr>'; $i++; endforeach; endif; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <footer class="panel-footer">
                <button class="btn btn-default" type="button" onclick="goBack(1);">Close</button>
            </footer>
        </section>
        <?php echo form_close(); ?>
    </div>
</div>

<!--  Begin Modal Form Detail -->
<div style="display:none;" class="k-edit-form-container" id="detailForm">
    <div class="col-md-12">
        <div class="form-group">
            <div class="col-md-5">
                <input id="RecordFlag" type="hidden" />
                <input id="RecordIDDetail" type="hidden" />
                <div class="k-edit-label">#</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="RowIndex" datarequired="0" readonly />
                </div>
                <div class="k-edit-label">Book ID</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="ItemID" datarequired="0" readonly />
                </div>
                <div class="k-edit-label">Book Name</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="ItemName" datarequired="0" readonly />
                </div>
                <div class="k-edit-label">Category</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="ItemType" datarequired="0" readonly />
                </div>
                <div class="k-edit-label">Location ID</div>
                <div class="k-edit-field">
                    <input type="text" id="LocationID" datarequired="1" class="k-input k-textbox" readonly />
                </div>
                <div class="k-edit-label">Location Name</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="LocationName" datarequired="0" readonly/>
                </div>
            </div>
            <div class="col-md-5">
                <div class="k-edit-label">Serial Number</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox " style="text-transform: uppercase" id="EPC" datarequired="1" readonly/>
                </div>
                <div class="k-edit-label">Barcode</div>
                <div class="k-edit-field">
                    <input type="text" class="k-input k-textbox" id="Barcode" datarequired="0" readonly/>
                </div>
                <div class="k-edit-label">Qty</div>
                <div class="k-edit-field">
                    <input type="text" class="" id="Qty" datarequired="0" readonly/>
                </div>
                <div class="k-edit-label">Remarks</div>
                <div class="k-edit-field">
                    <textarea class="k-textbox" id="RemarksDetail" readonly></textarea>
                </div>
            </div>
        </div>
        <div id="tableDetailModalSub" style="overflow:auto; display:none;">
            <table id="table-detailSub" class="table table-responsive">
                <thead id="head-detailSub">
                    <tr>
                        <th width="80px">Action</th>
                        <!--<th data-col="RowIndex2" class="th-RowIndex">#</th>-->
                        <th data-col="SerialNo">SerialNo</th>
                    </tr>
                </thead>
                <tbody id="list-detailSub">
                </tbody>
            </table>
        </div>

        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetail" class="btn btn-primary close-button" onclick="adddetail('detail');" style="display:none;"><i class="fa fa-save"></i> &nbsp;Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailForm');"><i class="fa fa-cancel"></i> &nbsp;Close</button>
        </div>
    </div>
</div>

<div style="display:none;" class="k-edit-form-container" id="detailSubForm">
    <div class="vol-mb-12">

 		<!--<div class="k-edit-label">#</div>
        <div class="k-edit-field">
            <input type="text" class="" id="RowIndex2" datarequired="0" readonly />
        </div>-->
 		<div class="k-edit-label">SerialNo</div>
        <div class="k-edit-field">
            <input type="text" class="k-input k-textbox" id="SerialNo" style="text-transform: uppercase" datarequired="0" />
        </div>
 		
        <div class="k-edit-buttons k-state-default">
            <button id="submitButtondetailSub" class="btn btn-primary close-button" onclick="adddetail('detailSub');"><i class="fa fa-save"></i> &nbsp;Save</button>
            <button class="btn btn-default close-button" onclick="CloseModal('detailSubForm');"><i class="fa fa-cancel"></i> &nbsp;Cancel</button>
        </div>
    </div>
</div>
<!--  End Modal Form Detail -->

<?php
//Item Lookup
    //field in database data to load
    $dataItem = array(
        array('field' => T_MasterDataBook_BookID, 'title' => 'Item ID', 'width' => '80px'),
        array('field' => T_MasterDataBook_BookTitle, 'title' => 'Item Name', 'width' => '150px'),
        array('field' => T_MasterDataBook_BookCategories, 'title' => 'Type', 'width' => '50px'),
    );

    //Double Click Throw Data to Form
    $columnItem = array(
        array('id' => 'ItemID', 'column' => T_MasterDataBook_BookID),
        array('id' => 'ItemName', 'column' => T_MasterDataBook_BookTitle),
        array('id' => 'ItemType', 'column' => T_MasterDataBook_BookCategories),
        array('id' => 'EPC', 'column' => T_MasterDataBook_BookSN),
        array('id' => 'Barcode', 'column' => T_MasterDataBook_BookBarcode),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("ItemID", "Data Item", "600px", "Webservice/Read/Getlistitem", $dataItem, $columnItem, T_MasterDataItem);

//Location Lookup
    //field in database data to load
    $dataLocation = array(
        array('field' => T_MasterDataLocation_LocationID, 'title' => 'Location ID', 'width' => '100px'),
        array('field' => T_MasterDataLocation_LocationName, 'title' => 'Location Name', 'width' => '100px'),
    );

    //Double Click Throw Data to Form
    $columnLocation = array(
        array('id' => 'LocationID', 'column' => T_MasterDataLocation_LocationID),
        array('id' => 'LocationName', 'column' => T_MasterDataLocation_LocationName),
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("Location", "Data Location", "500px", "Webservice/Read/Getlist", $dataLocation, $columnLocation,T_MasterDataLocation);

?>
<script type="text/javascript" src="assets/backend/javascripts/forms/table.detail.lib.js"></script>
<script type="text/javascript" src="assets/js/apps.js"></script>

<?php 

if (isset($t1010r001)) {
    $ID = $t1010r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script>
var ID = "<?php echo $ID; ?>";
var validasi = "<?php echo $validasi; ?>";
var LSTable = [
    {
        tbodyID: "list-detail",
        detailPrefix: "detail",
        lsID: current_url()+"detail",
        element: <?php echo json_encode($detail); ?>
    },
    {
        tbodyID: "list-detailSub",
        detailPrefix: "detailSub",
        lsID: current_url()+"detailSub",
        element: ""
    }
];
$(document).ready(function() {

    $("#RowIndex").kendoNumericTextBox(); 
    $("#RowIndex2").kendoNumericTextBox(); 
    $("#Qty").kendoNumericTextBox(); 

    kendoModal("detailForm","Add Detail","800px");
    kendoModal("detailSubForm","Add Item","400px");

     $("#DetailModalSub").click(function() {
        $("#detailSubForm").data("kendoWindow").center().open();
        var epc = $("#EPC").val();
        cleardetail("detailSub", 0);
        $("#SerialNo").val(epc);
    });

});

function insert()
    {
        var detail = getDetailSubItem('detail');
        var voData = {
            DocDate: $('#DocDate').val(),
            DocStatus: $('#DocStatus').val(),
            Remarks: $('#Remarks').val(),
            detail: detail
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Stockmovement/Beginbalance/Insert'),
                success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var detail = getDetailSubItem('detail');
     var voData = {
         RecordID: ID,
         TimeStamp: $('#TimeStamp').val(),
         DocNo: $('#DocNo').val(),
         DocDate: $('#DocDate').val(),
         DocStatus: $('#DocStatus').val(),
         Remarks: $('#Remarks').val(),
         DoRemoveID : $("#DoRemoveID").val(),
         detail: detail,
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Stockmovement/Beginbalance/Update'); ?>",
               success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    lsClear();
                    new PNotify({ title: "Success", text: result.msg, type: 'success', shadow: true });
                    window.location.replace(site_url('Stockmovement/Beginbalance/'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.DocDate == "") { valid = 0; msg += "Doc Date is required" + "\r\n"; }
    if (voData.detail == "") { valid = 0; msg += "Detail Data is required" + "\r\n"; }


    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}

//Sum Total
function sumTotal(target){
    var field = getDetailField(target);
    var val   = getDetailItem(target);
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);

    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}

</script>