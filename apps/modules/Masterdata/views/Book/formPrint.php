
        <div class="invoice">
            <header class="clearfix">
                <div class="row">
                    <?php //$this->navigations->kopsurat(); ?>
                </div>
            </header>
            <div class="bill-info">
                <div class="row">
                    <div class="col-md-6">
                        <div class="bill-data text-left">
                            <p class="mb-none">
                                <span class="text-dark">Date:</span>
                                <span class="value"><?php echo date(FORMATDATE,strtotime(${T_TransactionStockMovementHeader_DocDate})); ?></span>
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="bill-data text-Left">
                            <p class="mb-none">
                                <span class="text-dark"</span>
                                <span class="value"></span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix">
                <div class="row">
                    <div class="col-sm-12 text-center mt-md mb-md">
                        <h4>STOCK BALANCE : <?php echo ${T_TransactionStockMovementHeader_DocNo}; ?></h4>
                    </div>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table invoice-items">
                    <thead id="head-detail">
                        <tr>
                            <th data-col="RowIndex">#</th>
                            <th data-col="ItemID">Item ID</th>
                            <th data-col="ItemName">Item Name</th>
                            <th data-col="ItemType">Item Type</th>
                            <th data-col="Qty">Qty</th>
                            <th data-col="LocationID">Loc ID</th>
                            <th data-col="LocationName">Loc Name</th>
                            <th data-col="EPC">EPC</th>
                            <th data-col="Barcode">Barcode</th>
                            <th data-col="RemarksDetail">Remarks</th>
                            <th data-col="RecordIDDetail" style="display:none;"></th>
                            <th data-col="RecordFlag" style="display:none;"></th>
                        </tr>
                    </thead>
                    <tbody id="list-detail">
                        <?php $i=1; $target="'detail'"; $detail=""; if(isset($Detail) && !empty($Detail)): $dataDetail=1 ; 
                        foreach($Detail as $item): echo '<tr id="detail-'.$i. '">
                        <td id="detailRowIndexv-'.$i. '" data-val="'.$i. '">'.$i. '</td>
                        <td id="detailItemIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_ItemID]. '">'.$item[T_TransactionStockMovementDetail_ItemID]. '</td>
                        <td id="detailItemNamev-'.$i. '" data-val="'.$item[T_MasterDataBook_BookTitle]. '">'.$item[T_MasterDataBook_BookTitle]. '</td>
                        <td id="detailItemTypev-'.$i. '" data-val="'.$item[T_MasterDataBook_BookCategories]. '">'.$item[T_MasterDataBook_BookCategories]. '</td>
                        <td id="detailQtyv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Quantity1]. '">'.$item[T_TransactionStockMovementDetail_Quantity1]. '</td>
                        <td id="detailLocationIDv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_LocationID1]. '">'.$item[T_TransactionStockMovementDetail_LocationID1]. '</td>
                        <td id="detailLocationNamev-'.$i. '" data-val="'.$item[T_MasterDataLocation_LocationName]. '">'.$item[T_MasterDataLocation_LocationName]. '</td>
                        <td id="detailEPCv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_EPC]. '">'.$item[T_TransactionStockMovementDetail_EPC]. '</td>
                        <td id="detailBarcodev-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Barcode]. '">'.$item[T_TransactionStockMovementDetail_Barcode]. '</td>
                        <td id="detailRemarksDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_Remarks]. '">'.$item[T_TransactionStockMovementDetail_Remarks]. '</td>
                        <td id="detailRecordIDDetailv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordID]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordID]. '</td>
                        <td id="detailRecordFlagv-'.$i. '" data-val="'.$item[T_TransactionStockMovementDetail_RecordFlag]. '" style="display:none;">'.$item[T_TransactionStockMovementDetail_RecordFlag]. '</td>
                    </tr>'; $i++; endforeach; endif; ?>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <div class="bill-data text-Left">
                    <p class="mb-none">
                        <span class="text-dark">Remarks:</span>
                        <span class="value"><?php echo (empty(${T_TransactionStockMovementHeader_Remarks}))? ${T_TransactionStockMovementHeader_Remarks} : "-" ?></span>
                    </p>
                </div>
            </div>
           
        </div>

        <script>
            window.print();
        </script>
   