<style type="text/css">.mrg{margin-bottom:20px;}</style>
<div class="row">
    <div class="col-md-12">
        <section class="panel-primary">
            <header class="panel-heading">
                <div id="panel-doc" class="panel-actions">
                </div>
                <h2 class="panel-title"><?= isset(${T_MasterDataBook}) ? 'Edit': 'Add New' ?></h2>
            </header>
            <div class="panel-body">
                <input type="hidden" id="RecordID" value="<?php echo isset(${T_MasterDataBook_RecordID}) ? ${T_MasterDataBook_RecordID} : ''; ?>">
                <input type="hidden" id="TimeStamp" value="<?php echo isset(${T_MasterDataBook_RecordTimestamp}) ? ${T_MasterDataBook_RecordTimestamp} : ''; ?>">
                <div class="form-group">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">ID Buku</label>
                            <div class="col-md-6">
                                <?php $books=array( 
                                    'id'=> 'ItemID', 
                                    'class' => 'k-input k-textbox', 
                                    'style' => 'text-transform: uppercase;',
                                    'value' => isset(${T_MasterDataBook_BookID}) ? ${T_MasterDataBook_BookID} : "",
                                    'readonly' => "true"
                                ); 
                                if(!isset(${T_MasterDataBook_BookID})){ unset($books['readonly']); }
                                echo form_input($books); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Judul Buku</label>
                            <div class="col-md-6">
                                <?php $books=array(
                                    'id'=> 'BookTitle',
                                    'class' => 'k-input k-textbox',
                                    'style' => 'width:250px;',
                                    'value' => isset(${T_MasterDataBook_BookTitle}) ? ${T_MasterDataBook_BookTitle} : "",
                                ); 
                                echo form_input($books); ?>
                            </div>
                        </div><br>
                        <div class="row">
                            <label class="col-md-3 form-label">Kategori</label>
                            <div class="col-md-4">
                                <?php $books=array( 
                                    'id'=> 'BookCategories', 
                                    'class'=>'k-input', 
                                    'value' => isset(${T_MasterDataBook_BookCategories}) ? ${T_MasterDataBook_BookCategories} : "",
                                    //'readonly' => true  
                                ); 
                                echo form_input($books); ?>
                                </div>
                        </div><br>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Penulis</label>
                            <div class="col-md-9">
                                <input id="BookGroupAuthorID" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataBook_GroupAuthorID}) ? ${T_MasterDataBook_GroupAuthorID} : ''; ?>" />
                                <?php $books=array(
                                    'id'=> 'BookGroupName', 
                                    'class' => 'k-input k-textbox', 
                                    'value' => isset(${T_MasterDataBook_GroupAuthorID}) ? ${T_MasterDataBook_GroupAuthorID} : "",
                                    'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($books); echo"&nbsp;"; ?>
                                <div class="k-button" id="LookupEventGroupAuthorID"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                        <div class="row mrg">
                            <label class="col-md-3 form-label">Penerbit</label>
                            <div class="col-md-9">
                                <input id="BookGroupIDpenerbit" type="hidden" style="width:50px;background-color:#eee;" readonly="TRUE"  class="k-textbox form-control"  value="<?php echo isset(${T_MasterDataBook_GroupPublisher}) ? ${T_MasterDataBook_GroupPublisher} : ''; ?>" />
                                <?php $books=array(
                                    'id'=> 'BookGroupNamepenerbit', 
                                    'class' => 'k-input k-textbox', 
                                    'value' => isset(${T_MasterDataBook_GroupPublisher}) ? ${T_MasterDataBook_GroupPublisher} : "",
                                    'style' => 'background-color:#eee;margin-left:-5px;', 
                                    'readonly' => 'TRUE' 
                                ); 
                                echo form_input($books); echo"&nbsp;"; ?>
                                <div class="k-button" id="LookupEventGroupPublisher"> <div class="k-icon k-i-search"></div> </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="col-md-6">
                            <div class="row">
                                <label class="col-md-3 form-label">Picture</label>
                            <input type="hidden" id="photo" value="<?php echo isset($meta{T_MasterDataBook_BookPicture}) ? $meta{T_MasterDataBook_BookPicture} : ''; ?>">
                            <?php
                                if(isset(${T_MasterDataBook_BookPicture})){
                                    $voImg = (${T_MasterDataBook_BookPicture} == '') ? "assets/backend/images/buku.png" : ${T_MasterDataBook_BookPicture};
                                }else{
                                    $voImg = "assets/upload/photo/buku.png";
                                }
                            ?>
                            <div class="col-md-6">
                                <a id="thumbnail-photo" href="<?php echo $voImg; ?>" data-plugin-lightbox data-plugin-options='{ "type":"image" }' title="User Photo">
                                    <div id="preview" style="padding-left: 10px;padding-top: 10px;background-color:#eee;border-radius: 5%;width:180px;height:180px;">
                                        <img class="img-responsive" src="<?php echo $voImg; ?>" style="width:160px;height:160px;margin-bottom:10px;border-radius:5%;">
                                    </div>
                                </a>
                                <form id="form-photo" action="<?php echo site_url('en/System/User/upload'); ?>" method="post" style="display: inline-flex; position: absolute;" enctype="multipart/form-data">
                                    <input id="uploadImage" type="file" accept="image/*" name="image" value="<?php echo isset(${T_MasterDataBook_BookPicture}) ? ${T_MasterDataBook_BookPicture} : ''; ?>" />
                                    <input name="ItemIDS" id="ItemIDS" value="" type="hidden"/> 
                                    <input id="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-success" type="submit" value="Upload">
                                    <input type="hidden" id="PictureExist" value="<?php echo isset(${T_MasterDataBook_BookPicture}) ? ${T_MasterDataBook_BookPicture} : ''; ?>" />
                                </form>
                                <div id="err"></div>
                            </div>
                         </div>
                    </div>
                </div>
            </div>
                <div class="form-group mrg">
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">Barcode</label>
                            <div class="col-md-6">
                                <?php $books=array(
                                    'id'=> 'BookBarcode',
                                    'class' => 'k-input k-textbox',
                                    'style' => 'width:250px;',
                                    'value' => isset(${T_MasterDataBook_BookBarcode}) ? ${T_MasterDataBook_BookBarcode} : "",
                                ); 
                                echo form_input($books); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <label class="col-md-3 form-label">ISBN</label>
                            <div class="col-md-6">
                                <?php $books=array( 
                                    'id'=> 'BookISBN', 
                                    'class' => 'k-input k-textbox', 
                                    'style' => 'text-transform: uppercase;',
                                    'value' => isset(${T_MasterDataBook_BookISBN}) ? ${T_MasterDataBook_BookISBN} : "",
                                    // 'readonly' => "true"
                                ); 
                                echo form_input($books); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mrg">
                    <div class="col-md-6">    
                        <div class="row">
                            <label class="col-md-3 form-label">Tahun Diterbitkan</label>
                            <div class="col-md-6">
                                <?php $books=array(
                                    'id'=> 'BookYearpublished',
                                    'class' => 'k-input k-textbox',
                                    'style' => 'width:100px;',
                                    'value' => isset(${T_MasterDataBook_BookYearpublished}) ? ${T_MasterDataBook_BookYearpublished} : "",
                                ); 
                                echo form_input($books); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">     
                         <div class="row">
                            <label class="col-md-3 form-label">SN</label>
                            <div class="col-md-6">
                                <?php $books=array(
                                    'id'=> 'BookSN',
                                    'class' => 'k-input k-textbox',
                                    'style' => 'width:250px;',
                                    'value' => isset(${T_MasterDataBook_BookSN}) ? ${T_MasterDataBook_BookSN} : "",
                                ); 
                                echo form_input($books); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <label class="col-md-3 form-label">Kata Kunci</label>
                            <div class="col-md-6">
                                <?php $books=array(
                                    'id'=> 'BookKeywords',
                                    'class' => 'k-input k-textbox',
                                    'style' => 'width:300px;',
                                    'value' => isset(${T_MasterDataBook_BookKeywords}) ? ${T_MasterDataBook_BookKeywords} : "",
                                ); 
                                echo form_input($books); ?>
                            </div>
                        </div>
                    </div>
                </div>

            <footer class="panel-footer">
                <a href="javascript:void(0)" onclick="<?php if(isset(${T_MasterDataBook_RecordID})){echo"update()";}else{echo"insert()";};?>" class="btn btn-primary" >Save</a>
                <button class="btn btn-default" type="button" onclick="goBack(1);">Cancel</button>
            </footer>
        </section>
    </div>
</div>
<?php
//Person Type Lookup
    //field in database data to load
    $dataBookGroup = array(
        array('field' => T_MasterDataBookGroupAuthor_ID, 'title' => 'ID', 'width' => '100px'),
        array('field' => T_MasterDataBookGroupAuthor_Title, 'title' => 'Name', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnBookGroup = array(
        array('id' => 'BookGroupAuthorID', 'column' => T_MasterDataBookGroupAuthor_ID),
        array('id' => 'BookGroupName', 'column' => T_MasterDataBookGroupAuthor_Title),
    );

    $customfilter = array("");

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("GroupAuthorID", "Book Author", "500px", "Webservice/Read/Getlist", $dataBookGroup, $columnBookGroup,T_MasterDataBookGroupAuthor,"",$customfilter);

//Person Type Lookup
    //field in database data to load
    $dataBookPenerbit = array(
        array('field' => T_MasterDataBookGroupPublisher_ID, 'title' => 'ID', 'width' => '100px'),
        array('field' => T_MasterDataBookGroupPublisher_Name, 'title' => 'Name', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnBookPenerbit = array(
        array('id' => 'BookGroupIDpenerbit', 'column' => T_MasterDataBookGroupPublisher_ID),
        array('id' => 'BookGroupNamepenerbit', 'column' => T_MasterDataBookGroupPublisher_Name),
    );

    $customfilterPenerbit = array("");

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("GroupPublisher", "Book Publisher", "500px", "Webservice/Read/Getlist", $dataBookPenerbit, $columnBookPenerbit,T_MasterDataBookGroupPublisher,"",$customfilterPenerbit);

//Person Type Lookup
    //field in database data to load
    $dataIdentity = array(
        array('field' => T_MasterDataGeneralTableValue_Key, 'title' => 'Key', 'width' => '100px'),
        array('field' => T_MasterDataGeneralTableValue_Description, 'title' => 'Description', 'width' => '100px')
    );

    //Double Click Throw Data to Form
    $columnIdentity = array(
        array('id' => 'IdentityTypeID', 'column' => T_MasterDataGeneralTableValue_RecordID),
        array('id' => 'IdentityTypeName', 'column' => T_MasterDataGeneralTableValue_Description),
    );

    $customfilter2 = array(
        T_MasterDataGeneralTableValue_PRI => "5",
    );

    //id, title, size, URL, data field in database, Throw Data To form when click, Database Name
    echo kendoModalLookup("IdentityID", "Identity", "500px", "Webservice/Read/Getlist", $dataIdentity, $columnIdentity,T_MasterDataGeneralTableValue,"",$customfilter2);
?>

<?php
if (isset($t8040r001)) {
    $ID = $t8040r001;
    $validasi = "update";
}else{
    $ID = "";
    $validasi = "insert";
}
?>
<script type="text/javascript">
var ID = <?php echo isset(${T_MasterDataBook_RecordID}) ? ${T_MasterDataBook_RecordID} : 0; ?>;
$(document).ready(function() {
    $("#BookCategories").kendoDropDownList({
        filter: "contains",
        dataTextField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        dataValueField: "<?php echo T_MasterDataGeneralTableValue_Key; ?>",
        optionLabel: "Select Kategori",
        dataSource: {
            type: "json",
            transport: {
                read: {
                    url: site_url("Webservice/Generallist/getCountry"),
                    data: {table: '<?php echo T_MasterDataGeneralTableValue; ?>', filter: '<?php echo T_MasterDataGeneralTableValue_PRI; ?>', filval: 6},
                    type: "POST"
                }
            },
            schema: {
                data: function(data){
                    return data.data;
                }
            }
        }
    });
});

function processData()
{
    if(ID){
        update();
    }else{
        insert();
    }
}
function insert()
    {
        var voData = {
            BookID: $('#ItemID').val(),
            BookTitle: $('#BookTitle').val(),
            BookISBN: $('#BookISBN').val(),
            BookKeywords: $('#BookKeywords').val(),
            BookSN: $('#BookSN').val(),
            BookBarcode: $('#BookBarcode').val(),
            BookYearpublished: $('#BookYearpublished').val(),
            BookCategories: $('#BookCategories').val(),
            BookPicture: $('#photo').val(),
            GroupAuthorID: $('#BookGroupName').val(),
            GroupPublisher: $('#BookGroupNamepenerbit').val(),
            Color: $('#Color').val(),
            ColorCode: $('#picker').val(),
            Size: $('#Size').val()
        };
        var valid = checkForm(voData);
        if(valid.valid)
        {
            $.ajax({
                type: 'POST',
                data: voData,
                url:  site_url('Masterdata/Book/insert'),
                success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(current_url());
                }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(jQuery.parseJSON(jqXHR.responseText));
                }
            });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Update
    function update()
    {
     var voData = {
        RecordID: $('#RecordID').val(),
        TimeStamp: $('#TimeStamp').val(),
        BookID: $('#ItemID').val(),
        BookTitle: $('#BookTitle').val(),
        BookISBN: $('#BookISBN').val(),
        BookKeywords: $('#BookKeywords').val(),
        BookSN: $('#BookSN').val(),
        BookBarcode: $('#BookBarcode').val(),
        BookYearpublished: $('#BookYearpublished').val(),
        BookCategories: $('#BookCategories').val(),
        BookPicture: $('#photo').val(),
        GroupAuthorID: $('#BookGroupName').val(),
        GroupPublisher: $('#BookGroupNamepenerbit').val(),
        BookPictureExist: $("#PictureExist").val()
     };
     var valid = checkForm(voData);
         if(valid.valid)
         {
            $.ajax({
                type: 'POST',
                data: voData,
                url: "<?php echo site_url('Masterdata/Book/update'); ?>",
               success: function (result) {
                if (result.errorcode != 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                    new PNotify({ title: "Success", text: "Succes", type: 'success', shadow: true });
                    window.location.replace(site_url('Masterdata/Book/'));
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
               alert(jQuery.parseJSON(jqXHR.responseText));
           }
       });
        }else{
            new PNotify({ title: "Form Validation", text: valid.msg, type: 'error', shadow: true });
        }
    }

//Check Form
function checkForm(voData) {
    var valid = 1;
    var msg = "";

    if (voData.BookID == "") { valid = 0; msg += "Book ID is required" + "\r\n"; }
    if (voData.BookTitle == "") { valid = 0; msg += "Judul Buku is required" + "\r\n"; }
    if (voData.BookCategories == "") { valid = 0; msg += "Kategori is required" + "\r\n"; }
    if (voData.GroupAuthorID == "") { valid = 0; msg += "Penulis is required" + "\r\n"; }
    if (voData.GroupPublisher == "") { valid = 0; msg += "Penerbit is required" + "\r\n"; }
    if (voData.BookBarcode == "") { valid = 0; msg += "Barkode is required" + "\r\n"; }
    if (voData.BookYearpublished == "") { valid = 0; msg += "Tahun Diterbitkan is required" + "\r\n"; }
    if (voData.BookISBN == "") { valid = 0; msg += "ISBN is required" + "\r\n"; }
    if (voData.BookKeywords == "") { valid = 0; msg += "Kata Kunci id Type is required" + "\r\n"; }
    if (voData.BookSN == "") { valid = 0; msg += "SN is required" + "\r\n"; }

    var voRes = {
        valid: valid,
        msg: msg
    }
    return voRes;
}
//Sum Total
function sumTotal(target){
}

function checkField(target){
    var msg = '';
    var field = getDetailField(target);
    var val   = getDetailItem(target);
        for (v = 0; v < val.length; v++) {
        if($("#"+field[i]).attr("primary") == "1"){
            if($("#"+field[i]).val() == val[v].RowIndex)
            {
                msg+="Row Index Sudah Ada"+"\r\n";
            }            
        }
    }
    return msg;
}
function customTriger(i){
    var typeItem = $('#ItemType').val();
    if(typeItem === 'SS'){
        $('#DetailModalSub').show()
        $('#tableDetailModalSub').show();
        $('#detailSubRemoveAll').show();
        $('#Qty').data('kendoNumericTextBox').readonly(true);
        var qty = document.getElementById("list-detailSub").rows.length;
        $('#Qty').data('kendoNumericTextBox').value(qty);

    }else{
        $('#DetailModalSub').hide();
        $('#tableDetailModalSub').hide();
        $('#detailSubRemoveAll').hide();
        if(typeItem === 'NS'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
        }else if(typeItem === 'S1'){
            $('#Qty').data('kendoNumericTextBox').readonly(true);
            $('#Qty').data('kendoNumericTextBox').value(1);
        }else if(typeItem === 'SN')
        {
            $('#Qty').data('kendoNumericTextBox').readonly(false);
            if(!i){
                $('#Qty').data('kendoNumericTextBox').value(0);
            }
        }
    }
}
</script>