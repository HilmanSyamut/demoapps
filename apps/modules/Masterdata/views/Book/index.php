<style type="text/css">
    .customer-photo {
        display: inline-block;
        width: 40px;
        height: 40px;
        border-radius: 50%;
        background-size: 40px 38px;
        background-position: center center;
        vertical-align: middle;
        line-height: 32px;
        box-shadow: inset 0 0 1px #999, inset 0 0 10px rgba(0,0,0,.2);
        margin-left: 5px;
    }

    .customer-name {
        display: inline-block;
        vertical-align: middle;
        line-height: 32px;
        padding-left: 3px;
    }
</style>
<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_MasterDataBook_RecordID  => array(0,0,'50px','Record ID',1,'string',1),
    T_MasterDataBook_RecordTimestamp  => array(0,0,'50px','Record TimeStamp',1,'string',1),
    T_MasterDataBook_BookPicture  => array(0,1,'35px','Cover',0,'picture',0),
    T_MasterDataBook_BookID  => array(1,1,'50px','ID Buku',1,'string',1),
    T_MasterDataBook_BookTitle  => array(1,1,'100px','Judul Buku',0,'string',0),
    T_MasterDataBook_BookCategories => array(1,1,'80px','Kategori',0,'string',0),
    T_MasterDataBook_GroupAuthorID => array(1,1,'50px','Penulis',0,'string',0),
    T_MasterDataBook_GroupPublisher => array(1,1,'50px','Penerbit',0,'string',0),
    T_MasterDataBook_BookKeywords => array(1,1,'50px','Kata Kunci',0,'string',0)
);
// Column DropdownList => |Text|URL|KEY|
// $dropdownlist = array(
// T_MasterDataItem_UOMID => array('UOMID','Masterdata/Generaltable/getItemType/1',T_MasterDataGeneralTableValue_Key),
// T_MasterDataGeneralTableValue_RecordID => array('AutoIDType','Masterdata/Generaltable/getItemBiz/2',T_MasterDataGeneralTableValue_Key)
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid-buku',
    'actBTN' => "75px",
    'postBTN' => "0",
    'table' => T_MasterDataBook,
    'tools' => array(
        T_MasterDataBook_RecordID,
        T_MasterDataBook_RecordTimestamp,
        T_MasterDataBook_BookID,
        T_MasterDataBook_BookTitle,
        T_MasterDataBook_BookCategories,
        T_MasterDataBook_GroupAuthorID,
        T_MasterDataBook_GroupPublisher,
        T_MasterDataBook_BookKeywords),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist, 
    'url' => array(
        'create' => 'Masterdata/Book/Insert',
        'read' => 'Webservice/Read/Getlist',
        'update' => 'Masterdata/Book/Update',
        'destroy' => 'Masterdata/Book/Delete',
        'form' => 'Masterdata/Book/Form',
        'post' => '',
        'unpost' => ''
    )
);
// generate gridView
echo onlygridview($attr); 
?>