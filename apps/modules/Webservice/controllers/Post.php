<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Salman Fariz
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * Transactions Modules
 *
 * Stock Adjust Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 09.10.2016	
 * @author	    Salman Fariz
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Post extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('Post_model');
	}

//Stock Balance
	public function PostStockBalance(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->get("ID"));		
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == TRUE) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						if($getDetailPost->num_rows() > 0){
							foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
								if ($value->t8010f003 == "NS") {
								$updateTotal = ($valueDetail->t1020f003 + 0);	
								}else{		
								$updateTotal = ($valueDetail->t1020f003 + $value->t1011f009);
								}
								$this->Post_model->UpdatePost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
								$activity_stocklist = array(
									'recordstatus'=> 1,
									'record_id'=> $value->t1011r001,
									'doctype'=> substr($value->t1010f002, 0, 2),
									'docno'=> $value->t1010f002,
									'IDItem'=> $value->t1011f003,
									'IDLoc'=> $value->t1011f007,
									'quantity'=> $value->t1011f009,
									);
								activity_stocklist($activity_stocklist);
								$activity_log = array(
									'msg'=> 'Post Update list Stock Balance',
									'kategori'=> 7,
									'jenis'=> 1,
									'object'=> $value->t1011f003
									);
								activity_log($activity_log);
							}
						}else{	
							$info->errorcode = 32;
							$info->msg = "Data Tidak Ditemukan";
						}
					}else if(check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == FALSE){
						if ($value->t8010f003 == "NS") {
							$updateTotal = 0;	
						}else{		
							$updateTotal = $value->t1011f009;
						}
						$id = $value->t1011f001;
						$data = array(
							't1020r002' 		=> date("Y-m-d g:i:s",now()),
							't1020r003' 		=> 0,
							't1020f001' 		=> $value->t1011f003,
							't1020f002' 		=> $value->t1011f007,
							't1020f003' 		=> $updateTotal,
							);
						$this->Post_model->InsertPost($id, $data);
						$activity_stocklist = array(
							'recordstatus'=> 1,
							'record_id'=> $value->t1011r001,
							'doctype'=> substr($value->t1010f002, 0, 2),
							'docno'=> $value->t1010f002,
							'IDItem'=> $value->t1011f003,
							'IDLoc'=> $value->t1011f007,
							'quantity'=> $value->t1011f009,
							);
						activity_stocklist($activity_stocklist); 	
						$activity_log = array(
							'msg'=> 'Post Insert list Stock Balance',
							'kategori'=> 7,
							'jenis'=> 1,
							'object'=> $value->t1011f003
							);
						activity_log($activity_log);
					}
				}
			}else{
				$info->errorcode = 32;
				$info->msg = "Data Tidak Ditemukan";
			}
			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

public function UnPostStockBalance(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;

			$getPost = $this->Post_model->GetPost($this->input->get("ID"));
			foreach ($getPost->result() as $key => $value) {
				$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
						$count[] = $valueDetail->t1020f003;
					} 	
			}
			$jumlah = count($count);
			$id = count($count);
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					if($getDetailPost->num_rows() > 0){
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
							if ($value->t8010f003 == "NS") {
							$updateTotal = ($valueDetail->t1020f003 - 0);
							}else{
							$updateTotal = ($valueDetail->t1020f003 - $value->t1011f009);	
							}
							if ($updateTotal < 0) {
								$id--;
								$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Unpost karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
							}
						}
					}	
				}
				if ($jumlah == $id) {
					foreach ($getPost->result() as $key => $value) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
							if ($value->t8010f003 == "NS") {
							$updateTotal = ($valueDetail->t1020f003 - 0);
							}else{
							$updateTotal = ($valueDetail->t1020f003 - $value->t1011f009);
							}
							$this->Post_model->UpdateUnPost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
							$activity_stocklist = array(
							'recordstatus'=> 0,
							'record_id'=> $value->t1011r001,
							'doctype'=> substr($value->t1010f002, 0, 2),
							'docno'=> $value->t1010f002,
							'IDItem'=> $value->t1011f003,
							'IDLoc'=> $value->t1011f007,
							'quantity'=> '-'.$value->t1011f009,
							);
							activity_stocklist($activity_stocklist); 	
							$activity_log = array(
								'msg'=> 'UnPost Update list Stock Balance',
								'kategori'=> 7,
								'jenis'=> 1,
								'object'=> $value->t1011f003
								);
							activity_log($activity_log);
						}
					}
					$output = array('errorcode' => 0, 'msg' => 'success');
				}
			}
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	//Post Stock Transfer
	public function PostStockTransfer(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;

			$getPost = $this->Post_model->GetPost($this->input->get("ID"));		
			if($getPost->num_rows() > 0){
				//count Row
				$jumlah = count($getPost->result());
				$checkcount = count($getPost->result());
				$checkRow = count($getPost->result());

				//Check Row In stock Item
				foreach ($getPost->result() as $key => $value) {
					if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == FALSE) {
						$checkRow--;
						$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Post karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity.');
					}
				}

				//Check Quantity in database
				if($checkcount == $checkRow){
					foreach ($getPost->result() as $key => $value) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						if($getDetailPost->num_rows() > 0){
							foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
								$updateTotal = ($valueDetail->t1020f003 - $value->t1011f009);
								if ($updateTotal < 0) {
									$checkcount--;
									$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Post karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
								}
							}
						}	
					}
				}

				//Post Data if all condition required
				if ($jumlah == $checkcount && $jumlah == $checkRow) {
					foreach ($getPost->result() as $key => $value) {
						if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f008) == TRUE) {

							//Get Quantity Location 1
							$getDetailPostLoc1 = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
							
							//Get Quantity Location 2
							$getDetailPostLoc2 = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f008);

							//Kurangin data yang ada di location 1
							if($getDetailPostLoc1->num_rows() > 0){
								foreach ($getDetailPostLoc1->result() as $keyDetail => $valueDetail1) {
									$updateTotalLoc1 = ($valueDetail1->t1020f003 - $value->t1011f009);
									$this->Post_model->UpdatePost($value->t1011f001, $valueDetail1->t1020r001, $updateTotalLoc1);								
									$activity_stocklist = array(
										'recordstatus'=> 1,
										'record_id'=> $value->t1011r001,
										'doctype'=> substr($value->t1010f002, 0, 2),
										'docno'=> $value->t1010f002,
										'IDItem'=> $valueDetail1->t1020f001,
										'IDLoc'=> $value->t1011f007,
										'quantity'=> '-'.$value->t1011f009,
									);
									activity_stocklist($activity_stocklist);
								}
							}else{	
								$info->errorcode = 32;
								$info->msg = "Data Tidak Ditemukan";
							}

							//Tambah data yang di Location 2
							if($getDetailPostLoc2->num_rows() > 0){
								foreach ($getDetailPostLoc2->result() as $keyDetail => $valueDetail2) {
									$updateTotalLoc2 = ($valueDetail2->t1020f003 + $value->t1011f009);
									$this->Post_model->UpdatePost($value->t1011f001, $valueDetail2->t1020r001, $updateTotalLoc2);									
									$activity_stocklist = array(
										'recordstatus'=> 1,
										'record_id'=> $value->t1011r001,
										'doctype'=> substr($value->t1010f002, 0, 2),
										'docno'=> $value->t1010f002,
										'IDItem'=> $valueDetail2->t1020f001,
										'IDLoc'=> $value->t1011f008,
										'quantity'=> $value->t1011f009,
									);
									activity_stocklist($activity_stocklist);
								}
							}else{	
								$info->errorcode = 32;
								$info->msg = "Data Tidak Ditemukan";
							}								
						}else if(check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f008) == FALSE){

							//Get Quantity Location 1
							$getDetailPostLoc1 = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);

							//Kurangin data yang ada di location 1
							if($getDetailPostLoc1->num_rows() > 0){
								foreach ($getDetailPostLoc1->result() as $keyDetail => $valueDetail) {
									$updateTotalLoc1 = ($valueDetail->t1020f003 - $value->t1011f009);
									$this->Post_model->UpdatePost($value->t1011f001, $valueDetail->t1020r001, $updateTotalLoc1);
									$activity_stocklist = array(
										'recordstatus'=> 1,
										'record_id'=> $value->t1011r001,
										'doctype'=> substr($value->t1010f002, 0, 2),
										'docno'=> $value->t1010f002,
										'IDItem'=> $valueDetail->t1020f001,
										'IDLoc'=> $value->t1011f007,
										'quantity'=> '-'.$value->t1011f009,
									);									
									activity_stocklist($activity_stocklist);
								}
							}else{
								$id = $value->t1011f001;
									$data = array(
										't1020r002' 		=> date("Y-m-d g:i:s",now()),
										't1020r003' 		=> 0,
										't1020f001' 		=> $value->t1011f003,
										't1020f002' 		=> $value->t1011f007,
										't1020f003' 		=> '-'.$value->t1011f009,
										);
									$this->Post_model->InsertPost($id, $data);		
									$activity_stocklist = array(
										'recordstatus'=> 1,
										'record_id'=> $value->t1011r001,
										'doctype'=> substr($value->t1010f002, 0, 2),
										'docno'=> $value->t1010f002,
										'IDItem'=> $value->t1011f003,
										'IDLoc'=> $value->t1011f007,
										'quantity'=> '-'.$value->t1011f009,
									);
									activity_stocklist($activity_stocklist);
							}
								$id = $value->t1011f001;
								$data = array(
									't1020r002' 		=> date("Y-m-d g:i:s",now()),
									't1020r003' 		=> 0,
									't1020f001' 		=> $value->t1011f003,
									't1020f002' 		=> $value->t1011f008,
									't1020f003' 		=> $value->t1011f009,
									);
								$this->Post_model->InsertPost($id, $data);		
								$activity_stocklist = array(
									'recordstatus'=> 1,
									'record_id'=> $value->t1011r001,
									'doctype'=> substr($value->t1010f002, 0, 2),
									'docno'=> $value->t1010f002,
									'IDItem'=> $value->t1011f003,
									'IDLoc'=> $value->t1011f008,
									'quantity'=> $value->t1011f009,
								);
								activity_stocklist($activity_stocklist);
							}
					}
					$output = array('errorcode' => 0, 'msg' => 'success');								
				}
			}else{
				$info->errorcode = 32;
				$info->msg = "Data Tidak Ditemukan";
			}
			// $output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPostStockTransfer(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;

			$getPost = $this->Post_model->GetPost($this->input->get("ID"));		
			if($getPost->num_rows() > 0){
				//count Row
				$jumlah = count($getPost->result());
				$checkcount = count($getPost->result());
				$checkRow = count($getPost->result());

				//Check Row In stock Item
				foreach ($getPost->result() as $key => $value) {
					if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f008) == FALSE) {
						$checkRow--;
						$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Unpost karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f008.' tidak memiliki stock Quantity.');
					}
				}

				//Check Quantity in database
				if($checkcount == $checkRow){
					foreach ($getPost->result() as $key => $value) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f008);
						if($getDetailPost->num_rows() > 0){
							foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
								$updateTotal = ($valueDetail->t1020f003 - $value->t1011f009);
								if ($updateTotal < 0) {
									$checkcount--;
									$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Unpost karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f008.' tidak memiliki stock Quantity barang yang cukup.');
								}
							}
						}	
					}
				}

				//Post Data if all condition required
				if ($jumlah == $checkcount && $jumlah == $checkRow) {
					foreach ($getPost->result() as $key => $value) {
						if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f008) == TRUE) {

							//Get Quantity Location 1
							$getDetailPostLoc1 = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
							
							//Get Quantity Location 2
							$getDetailPostLoc2 = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f008);

							//Tambah data yang ada di location 1
							if($getDetailPostLoc1->num_rows() > 0){
								foreach ($getDetailPostLoc1->result() as $keyDetail => $valueDetail1) {
									$updateTotalLoc1 = ($valueDetail1->t1020f003 + $value->t1011f009);
									$this->Post_model->UpdateUnPost($value->t1011f001, $valueDetail1->t1020r001, $updateTotalLoc1);
								$activity_stocklist = array(
									'recordstatus'=> 0,
									'record_id'=> $value->t1011r001,
									'doctype'=> substr($value->t1010f002, 0, 2),
									'docno'=> $value->t1010f002,
									'IDItem'=> $valueDetail1->t1020f001,
									'IDLoc'=> $valueDetail1->t1020f002,
									'quantity'=> $value->t1011f009,
								);
								activity_stocklist($activity_stocklist);
								}
							}else{	
								$info->errorcode = 32;
								$info->msg = "Data Tidak Ditemukan";
							}

							//Kurangin data yang di Location 2
							if($getDetailPostLoc2->num_rows() > 0){
								foreach ($getDetailPostLoc2->result() as $keyDetail => $valueDetail2) {
									$updateTotalLoc2 = ($valueDetail2->t1020f003 - $value->t1011f009);
									$this->Post_model->UpdateUnPost($value->t1011f001, $valueDetail2->t1020r001, $updateTotalLoc2);
								}
							}else{	
								$info->errorcode = 32;
								$info->msg = "Data Tidak Ditemukan";
							}			
							$activity_stocklist = array(
								'recordstatus'=> 0,
								'record_id'=> $value->t1011r001,
								'doctype'=> substr($value->t1010f002, 0, 2),
								'docno'=> $value->t1010f002,
								'IDItem'=> $valueDetail1->t1020f001,
								'IDLoc'=> $valueDetail2->t1020f002,
								'quantity'=> '-'.$value->t1011f009,
							);
							activity_stocklist($activity_stocklist);								
						}
					}
					$output = array('errorcode' => 0, 'msg' => 'success');								
				}
			}else{
				$info->errorcode = 32;
				$info->msg = "Data Tidak Ditemukan";
			}
			// $output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Take
	public function PostStockTake(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->get("ID"));		
			if($getPost->num_rows() > 0){
				//count Row
				// $jumlah = count($getPost->result());
				// $checkcount = count($getPost->result());
				// $checkRow = count($getPost->result());

				//Check Row In stock Item
				// foreach ($getPost->result() as $key => $value) {
				// 	if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == FALSE) {
				// 		$checkRow--;
				// 		$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Post karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity.');
				// 	}
				// }

				//Check Quantity in database
				// if($checkcount == $checkRow){
				// 	foreach ($getPost->result() as $key => $value) {
				// 		$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
				// 		if($getDetailPost->num_rows() > 0){
				// 			foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
				// 				$updateTotal = (($value->t1011f009 - $value->t1011f010) + $valueDetail->t1020f003);
				// 				if ($updateTotal < 0) {
				// 					$checkcount--;
				// 					$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Post karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
				// 				}
				// 			}
				// 		}	
				// 	}
				// }

			//Post Data if all condition required
			// if ($jumlah == $checkcount && $jumlah == $checkRow) {
				foreach ($getPost->result() as $key => $value) {
				$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
				if($getDetailPost->num_rows() > 0){
					foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
						if ($value->t1011f009 == $valueDetail->t1020f003) {
								$updateTotal = ($valueDetail->t1020f003);
								$this->Post_model->UpdatePost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
									$activity_log = array(
										'msg'=> 'Post Update list Stock Take',
										'kategori'=> 7,
										'jenis'=> 1,
										'object'=> $value->t1011f003
										);
									activity_log($activity_log);
							}else{
								$updateTotal = (($value->t1011f009 - $value->t1011f010) + $valueDetail->t1020f003);
								$this->Post_model->UpdatePost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);	
									$activity_log = array(
										'msg'=> 'Post Update list Stock Take',
										'kategori'=> 7,
										'jenis'=> 1,
										'object'=> $value->t1011f003
										);
									activity_log($activity_log);
							}
							$activity_stocklist = array(
								'recordstatus'=> 1,
								'record_id'=> $value->t1011r001,
								'doctype'=> substr($value->t1010f002, 0, 2),
								'docno'=> $value->t1010f002,
								'IDItem'=> $value->t1011f003,
								'IDLoc'=> $value->t1011f007,
								'quantity'=> ($value->t1011f009 - $value->t1011f010),
								);
							activity_stocklist($activity_stocklist);
						}
					}else{
							$id = $value->t1011f001;
							$data = array(
								't1020r002' 		=> date("Y-m-d g:i:s",now()),
								't1020r003' 		=> 0,
								't1020f001' 		=> $value->t1011f003,
								't1020f002' 		=> $value->t1011f007,
								't1020f003' 		=> ($value->t1011f009 - $value->t1011f010),
								);
							$this->Post_model->InsertPost($id, $data);
							$activity_stocklist = array(
								'recordstatus'=> 1,
								'record_id'=> $value->t1011r001,
								'doctype'=> substr($value->t1010f002, 0, 2),
								'docno'=> $value->t1010f002,
								'IDItem'=> $value->t1011f003,
								'IDLoc'=> $value->t1011f007,
								'quantity'=> ($value->t1011f009 - $value->t1011f010),
								);
							activity_stocklist($activity_stocklist); 	
							$activity_log = array(
								'msg'=> 'Post Insert list Stock Balance',
								'kategori'=> 7,
								'jenis'=> 1,
								'object'=> $value->t1011f003
								);
							activity_log($activity_log);
							}
						}
						$output = array('errorcode' => 0, 'msg' => 'success');
				// }
			}else{
				$info->errorcode = 32;
				$info->msg = "Data Tidak Ditemukan";
			}
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPostStockTake(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;

			$getPost = $this->Post_model->GetPost($this->input->get("ID"));
			// foreach ($getPost->result() as $key => $value) {
			// 	$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
			// 		foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
			// 			$count[] = $valueDetail->t1020f003;
			// 		} 	
			// }
			// $jumlah = count($count);
			// $id = count($count);
			if($getPost->num_rows() > 0){
				// foreach ($getPost->result() as $key => $value) {
				// 	$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
				// 	if($getDetailPost->num_rows() > 0){
				// 		foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
				// 			$updateTotal = $valueDetail->t1020f003 - ($value->t1011f009 - $value->t1011f010);
				// 			if ($updateTotal < 0) {
				// 				$id--;
				// 				$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Unpost karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
				// 			}
				// 		}
				// 	}	
				// }

				// if ($jumlah == $id) {
					foreach ($getPost->result() as $key => $value) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
							$updateTotal = ($valueDetail->t1020f003 - ($value->t1011f009 - $value->t1011f010));
							$this->Post_model->UpdateUnPost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
							$activity_stocklist = array(
								'recordstatus'=> 0,
								'record_id'=> $value->t1011r001,
								'doctype'=> substr($value->t1010f002, 0, 2),
								'docno'=> $value->t1010f002,
								'IDItem'=> $value->t1011f003,
								'IDLoc'=> $value->t1011f007,
								'quantity'=> '-'.($value->t1011f009 - $value->t1011f010),
								);
							activity_stocklist($activity_stocklist);
							$activity_log = array(
								'msg'=> 'UnPost Update list Stock Balance',
								'kategori'=> 7,
								'jenis'=> 1,
								'object'=> $value->t1011f003
								);
							activity_log($activity_log);
						}
					}
					$output = array('errorcode' => 0, 'msg' => 'success');
				// }
			}
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Stock Adjust
		public function PostStockAdjust(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->get("ID"));		
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == TRUE) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						if($getDetailPost->num_rows() > 0){
							foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
								$updateTotal = ($valueDetail->t1020f003 + $value->t1011f010);
								$this->Post_model->UpdatePost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
								$activity_stocklist = array(
									'recordstatus'=> 1,
									'record_id'=> $value->t1011r001,
									'doctype'=> substr($value->t1010f002, 0, 2),
									'docno'=> $value->t1010f002,
									'IDItem'=> $value->t1011f003,
									'IDLoc'=> $value->t1011f007,
									'quantity'=> $value->t1011f010,
									);
								activity_stocklist($activity_stocklist);
								$activity_log = array(
									'msg'=> 'Post Update list Stock Adjust',
									'kategori'=> 7,
									'jenis'=> 1,
									'object'=> $value->t1011f003
									);
								activity_log($activity_log);
							}
						}else{	
							$info->errorcode = 32;
							$info->msg = "Data Tidak Ditemukan";
						}
					}else if(check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == FALSE){
						$id = $value->t1011f001;
						$data = array(
							't1020r002' 		=> date("Y-m-d g:i:s",now()),
							't1020r003' 		=> 0,
							't1020f001' 		=> $value->t1011f003,
							't1020f002' 		=> $value->t1011f007,
							't1020f003' 		=> $value->t1011f010,
							);
						$this->Post_model->InsertPost($id, $data);
						$activity_stocklist = array(
							'recordstatus'=> 1,
							'record_id'=> $value->t1011r001,
							'doctype'=> substr($value->t1010f002, 0, 2),
							'docno'=> $value->t1010f002,
							'IDItem'=> $value->t1011f003,
							'IDLoc'=> $value->t1011f007,
							'quantity'=> $value->t1011f010,
							);
						activity_stocklist($activity_stocklist); 	
						$activity_log = array(
							'msg'=> 'Post Insert list Stock Adjust',
							'kategori'=> 7,
							'jenis'=> 1,
							'object'=> $value->t1011f001
							);
						activity_log($activity_log);
					}
				}
			}else{
				$info->errorcode = 32;
				$info->msg = "Data Tidak Ditemukan";
			}
			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPostStockAdjust(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->get("ID"));
			foreach ($getPost->result() as $key => $value) {
				$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
						$count[] = $valueDetail->t1020f003;
					} 	
			}
			$jumlah = count($count);
			$id = count($count);
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					if($getDetailPost->num_rows() > 0){
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
							$updateTotal = ($valueDetail->t1020f003 - $value->t1011f010);
							if ($updateTotal < 0) {
								$id--;
								$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Unpost karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
							}
						}
					}	
				}
				if ($jumlah == $id) {
					foreach ($getPost->result() as $key => $value) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
							$updateTotal = ($valueDetail->t1020f003 - $value->t1011f010);
							$this->Post_model->UpdateUnPost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
							$activity_log = array(
								'msg'=> 'UnPost Update list Stock Balance',
								'kategori'=> 7,
								'jenis'=> 1,
								'object'=> $value->t1011f003
								);
							activity_log($activity_log);
							$activity_stocklist = array(
								'recordstatus'=> 0,
								'record_id'=> $value->t1011r001,
								'doctype'=> substr($value->t1010f002, 0, 2),
								'docno'=> $value->t1010f002,
								'IDItem'=> $value->t1011f003,
								'IDLoc'=> $value->t1011f007,
								'quantity'=> '-'.$value->t1011f010,
								);
							activity_stocklist($activity_stocklist);
						}
					}
					$output = array('errorcode' => 0, 'msg' => 'success');
				}
			}
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Sales
	public function PostSales(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->get("ID"));
			foreach ($getPost->result() as $key => $value) {
				$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
						$count[] = $valueDetail->t1020f003;
					} 	
			}
			if (empty($count)) {
				$jumlah = 300;
				$id = 200;
				$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Post karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
			}else{
				$jumlah = count($count);
				$id = count($count);
			}
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					if($getDetailPost->num_rows() > 0){
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
							if ($value->t8010f003 == "NS") {
								$updateTotal = ($valueDetail->t1020f003 - 0);
							}else{
								$updateTotal = ($valueDetail->t1020f003 - $value->t1011f009);	
							}
							if ($updateTotal < 0) {
								$id--;
								$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Post karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
							}
						}
					}	
				}
				if ($jumlah == $id) {
					foreach ($getPost->result() as $key => $value) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail1) {
							if ($value->t8010f003 == "NS") {
								$updateTotal = ($valueDetail1->t1020f003 - 0);
							}else{
								$updateTotal = ($valueDetail1->t1020f003 - $value->t1011f009);
							}
							$this->Post_model->UpdatePost($value->t1011f001, $valueDetail1->t1020r001, $updateTotal);
							$activity_stocklist = array(
							'recordstatus'=> 1,
							'record_id'=> $value->t1011r001,
							'doctype'=> substr($value->t1010f002, 0, 4),
							'docno'=> $value->t1010f002,
							'IDItem'=> $value->t1011f003,
							'IDLoc'=> $value->t1011f007,
							'quantity'=> '-'.$value->t1011f009,
							);
							activity_stocklist($activity_stocklist); 	
							$activity_log = array(
								'msg'=> 'UnPost Update list Stock Balance',
								'kategori'=> 7,
								'jenis'=> 1,
								'object'=> $value->t1011f003
								);
							activity_log($activity_log);
						}
					}
					$output = array('errorcode' => 0, 'msg' => 'success');
				}
			}
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPostSales(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->get("ID"));		
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == TRUE) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						if($getDetailPost->num_rows() > 0){
							foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
								if ($value->t8010f003 == "NS") {
								$updateTotal = ($valueDetail->t1020f003 + 0);	
								}else{		
								$updateTotal = ($valueDetail->t1020f003 + $value->t1011f009);
								}
								$this->Post_model->UpdateUnPost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
								$activity_stocklist = array(
									'recordstatus'=> 0,
									'record_id'=> $value->t1011r001,
									'doctype'=> substr($value->t1010f002, 0, 4),
									'docno'=> $value->t1010f002,
									'IDItem'=> $value->t1011f003,
									'IDLoc'=> $value->t1011f007,
									'quantity'=> $value->t1011f009,
									);
								activity_stocklist($activity_stocklist);
								$activity_log = array(
									'msg'=> 'Post Update list Stock Balance',
									'kategori'=> 7,
									'jenis'=> 1,
									'object'=> $value->t1011f003
									);
								activity_log($activity_log);
							}
						}else{	
							$info->errorcode = 32;
							$info->msg = "Data Tidak Ditemukan";
						}
					}else if(check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == FALSE){
						if ($value->t8010f003 == "NS") {
							$updateTotal = 0;	
						}else{		
							$updateTotal = $value->t1011f009;
						}
						$id = $value->t1011f001;
						$data = array(
							't1020r002' 		=> date("Y-m-d g:i:s",now()),
							't1020r003' 		=> 0,
							't1020f001' 		=> $value->t1011f003,
							't1020f002' 		=> $value->t1011f007,
							't1020f003' 		=> $updateTotal,
							);
						$this->Post_model->InsertPost($id, $data);
						$activity_stocklist = array(
							'recordstatus'=> 1,
							'record_id'=> $value->t1011r001,
							'doctype'=> substr($value->t1010f002, 0, 2),
							'docno'=> $value->t1010f002,
							'IDItem'=> $value->t1011f003,
							'IDLoc'=> $value->t1011f007,
							'quantity'=> $value->t1011f009,
							);
						activity_stocklist($activity_stocklist); 	
						$activity_log = array(
							'msg'=> 'Post Insert list Stock Balance',
							'kategori'=> 7,
							'jenis'=> 1,
							'object'=> $value->t1011f003
							);
						activity_log($activity_log);
					}
				}
			}else{
				$info->errorcode = 32;
				$info->msg = "Data Tidak Ditemukan";
			}
			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

//Purchase
	public function PostPurchase(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->post("ID"));		
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					if (check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == TRUE) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						if($getDetailPost->num_rows() > 0){
							foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
								if ($value->t8010f003 == "NS") {
								$updateTotal = ($valueDetail->t1020f003 + 0);	
								}else{		
								$updateTotal = ($valueDetail->t1020f003 + $value->t1011f009);
								}
								$this->Post_model->UpdatePost($value->t1011f001, $valueDetail->t1020r001, $updateTotal);
								$activity_stocklist = array(
									'recordstatus'=> 1,
									'record_id'=> $value->t1011r001,
									'doctype'=> substr($value->t1010f002, 0, 4),
									'docno'=> $value->t1010f002,
									'IDItem'=> $value->t1011f003,
									'IDLoc'=> $value->t1011f007,
									'quantity'=> $value->t1011f009,
									);
								activity_stocklist($activity_stocklist);
								$activity_log = array(
									'msg'=> 'Post Update list Stock Balance',
									'kategori'=> 7,
									'jenis'=> 1,
									'object'=> $value->t1011f003
									);
								activity_log($activity_log);
							}
						}else{	
							$info->errorcode = 32;
							$info->msg = "Data Tidak Ditemukan";
						}
					}else if(check_post('t1020f001', 't1020f002', $value->t1011f003, $value->t1011f007) == FALSE){
						if ($value->t8010f003 == "NS") {
							$updateTotal = 0;	
						}else{		
							$updateTotal = $value->t1011f009;
						}
						$id = $value->t1011f001;
						$data = array(
							't1020r002' 		=> date("Y-m-d g:i:s",now()),
							't1020r003' 		=> 0,
							't1020f001' 		=> $value->t1011f003,
							't1020f002' 		=> $value->t1011f007,
							't1020f003' 		=> $updateTotal,
							);
						$this->Post_model->InsertPost($id, $data);
						$activity_stocklist = array(
							'recordstatus'=> 1,
							'record_id'=> $value->t1011r001,
							'doctype'=> substr($value->t1010f002, 0, 4),
							'docno'=> $value->t1010f002,
							'IDItem'=> $value->t1011f003,
							'IDLoc'=> $value->t1011f007,
							'quantity'=> $value->t1011f009,
							);
						activity_stocklist($activity_stocklist); 	
						$activity_log = array(
							'msg'=> 'Post Insert list Stock Balance',
							'kategori'=> 7,
							'jenis'=> 1,
							'object'=> $value->t1011f003
							);
						activity_log($activity_log);
					}
				}
			}else{
				$info->errorcode = 32;
				$info->msg = "Data Tidak Ditemukan";
			}
			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPostPurchase(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
	
			$getPost = $this->Post_model->GetPost($this->input->post("ID"));
			foreach ($getPost->result() as $key => $value) {
				$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
						$count[] = $valueDetail->t1020f003;
					} 	
			}
			$jumlah = count($count);
			$id = count($count);
			if($getPost->num_rows() > 0){
				foreach ($getPost->result() as $key => $value) {
					$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
					if($getDetailPost->num_rows() > 0){
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail) {
							if ($value->t8010f003 == "NS") {
								$updateTotal = ($valueDetail->t1020f003 - 0);
							}else{
								$updateTotal = ($valueDetail->t1020f003 - $value->t1011f009);	
							}
							if ($updateTotal < 0) {
								$id--;
								$output = array('errorcode' => 400, 'msg' => 'Tidak dapat melakukan Unpost karna pada Item ID '.$value->t1011f003.' dan lokasi ID '.$value->t1011f007.' tidak memiliki stock Quantity barang yang cukup.');
							}
						}
					}	
				}
				if ($jumlah == $id) {
					foreach ($getPost->result() as $key => $value) {
						$getDetailPost = $this->Post_model->GetDetailPost($value->t1011f003, $value->t1011f007);
						foreach ($getDetailPost->result() as $keyDetail => $valueDetail1) {
							if ($value->t8010f003 == "NS") {
								$updateTotal = ($valueDetail1->t1020f003 - 0);
							}else{
								$updateTotal = ($valueDetail1->t1020f003 - $value->t1011f009);
							}
							$this->Post_model->UpdateUnPost($value->t1011f001, $valueDetail1->t1020r001, $updateTotal);
							$activity_stocklist = array(
							'recordstatus'=> 0,
							'record_id'=> $value->t1011r001,
							'doctype'=> substr($value->t1010f002, 0, 2),
							'docno'=> $value->t1010f002,
							'IDItem'=> $value->t1011f003,
							'IDLoc'=> $value->t1011f007,
							'quantity'=> '-'.$value->t1011f009,
							);
							activity_stocklist($activity_stocklist); 	
							$activity_log = array(
								'msg'=> 'UnPost Update list Stock Balance',
								'kategori'=> 7,
								'jenis'=> 1,
								'object'=> $value->t1011f003
								);
							activity_log($activity_log);
						}
					}
					$output = array('errorcode' => 0, 'msg' => 'success');
				}
			}
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file post.php */
/* Location: ./app/modules/webservice/controllers/post.php */
