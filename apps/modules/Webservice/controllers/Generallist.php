 <?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Master Module
 *
 * Generallist Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 14.12.2015	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Generallist extends BC_Controller 
{

	function __construct()
    {
    	parent::__construct();
    	$this->load->model('generallist/Generallist_model');
	}

	public function index()
	{
        print_out("webservice");
	}

	public function GetList(){
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$getList = $this->Generallist_model->getList();
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
		
	}

	public function Insert(){
		try{
			if(check_column(T_GENERALLIST_ID, 'NameCode') == TRUE)
			{
			 	$output = array('errorcode' => 200, 'msg' => 'Data dengan Group Name '.strtoupper($this->input->post("NameCode")).' sudah ada');
			}else{
				$data = array(
					T_GENERALLIST_RecordTimeStamp => date("Y-m-d H:i:s",now()),
					T_GENERALLIST_RecordStatus => 0,
					T_GENERALLIST_UpdateAt => now(),
					T_GENERALLIST_UpdateBy => $this->ezrbac->getCurrentUserID(),
					T_GENERALLIST_UpdateOn => $this->input->ip_address(),
					T_GENERALLIST_ID => $this->input->post("NameCode"),
					T_GENERALLIST_Name => $this->input->post("Name"),
					T_GENERALLIST_GROUPNAME => $this->input->post("GroupName"),
					T_GENERALLIST_GROUPCODE => $this->input->post("GroupCode"),
					T_GENERALLIST_STATUS => empty($this->input->post("Status")) ? 1 : $this->input->post("Status"),
				);
					
				$this->Generallist_model->Insert($data);
				$id = $this->db->insert_id();
	            $activity_log = array(
	                'msg'=> 'Insert new list',
	                'kategori'=> 7,
	                'jenis'=> 1,
	                'object'=> $id
	            );

	            activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}


	public function Update(){
		try{
			if(check_column(T_GENERALLIST_RecordTimeStamp, 'RecordTimeStamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$RecordID = $this->input->post("RecordID");
				$data = array(
					T_GENERALLIST_RecordTimeStamp => date("Y-m-d H:i:s",now()),
					T_GENERALLIST_RecordStatus => 0,
					T_GENERALLIST_UpdateAt => now(),
					T_GENERALLIST_UpdateBy => $this->ezrbac->getCurrentUserID(),
					T_GENERALLIST_UpdateOn => $this->input->ip_address(),
					T_GENERALLIST_ID => $this->input->post("NameCode"),
					T_GENERALLIST_Name => $this->input->post("Name"),
					T_GENERALLIST_GROUPNAME => $this->input->post("GroupName"),
					T_GENERALLIST_GROUPCODE => $this->input->post("GroupCode"),
					T_GENERALLIST_STATUS => empty($this->input->post("Status")) ? 1 : $this->input->post("Status"),
				);

				$this->Generallist_model->Update($RecordID, $data);

	            $activity_log = array(
	                'msg'=> 'Update list',
	                'kategori'=> 7,
	                'jenis'=> 2,
	                'object'=> $RecordID
	            );

	            activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}


	public function Delete()
	{
		try{
			$this->Generallist_model->Delete($this->input->post("RecordID"));
			$activity_log = array(
                'msg'=> 'Delete list',
                'kategori'=> 7,
                'jenis'=> 3,
                'object'=> $this->input->post("RecordID")
            );

            activity_log($activity_log);
			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function GetItemType()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$GroupCode = $this->input->post('GroupCode');
		$getList = $this->Generallist_model->DropdownList($GroupCode);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getcountry()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = $this->input->post('table');
		$filter = $this->input->post('filter');
		$filval = $this->input->post('filval');
		$getList = $this->Generallist_model->getCountry($table,$filter,$filval);
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
}

/* End of file generalist.php */
/* Location: ./app/modules/master/controllers/generalist.php */
