<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Generallist_model extends CI_Model
{
    public function getList(){
        $this->db->select('s.*');
        $this->db->from('t8000 s');
        $this->db->order_by('s.t8000r001');
        return $this->db->get();
    }

    public function Insert($data){
        $this->db->trans_begin();
        $this->db->insert("t8000",$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Update($RecordID, $data){
        $this->db->trans_begin();
        $this->db->where('t8000.t8000r001', $RecordID);
        $this->db->update("t8000",$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete('t8000', array('t8000r001' => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function DropdownList($GroupCode){
        $this->db->select('i.t8000f001');
        $this->db->from('t8000 as i');
        $this->db->where('i.t8000f004', $GroupCode);
        $this->db->where('i.t8000f005', 1);
        return $this->db->get();
    }

    public function getCountry($table,$filter="",$filval=""){
        if($filter)
        {
            $this->db->where($filter, $filval);
        }
        $this->db->order_by($table.'r001');
        return $this->db->get($table);
    }
}
