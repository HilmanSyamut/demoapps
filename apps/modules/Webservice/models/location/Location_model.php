<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends CI_Model
{    
    
    public function Insert($data){
        $this->db->trans_begin();
            unset($data[T_LOCATION_RecordID]);
            $this->db->insert(T_LOCATION,$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Update($RecordID,$data){
        $this->db->trans_begin();
            $this->db->where(T_LOCATION_RecordID, $RecordID);
            $this->db->update(T_LOCATION,$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function Delete($id){
        $this->db->trans_begin();
        
        $this->db->where(T_LOCATION_RecordID, $id);
        $this->db->delete(T_LOCATION);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}
