<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
    T_SystemNavigation_RecordID => array(0,0,'50px','Record ID',0,'string',0),
    T_SystemNavigation_RecordTimestamp => array(0,0,'50px','Record Timestamp',0,'string',0),
    T_SystemNavigation_RecordStatus => array(0,0,'50px','Record Status',0,'string',0),
    T_SystemNavigation_RecordUpdatedOn => array(0,0,'50px','Record Updated On',0,'string',0),
    T_SystemNavigation_RecordUpdatedBy => array(0,0,'50px','Record Updated By',0,'string',0),
    T_SystemNavigation_RecordUpdatedAt => array(0,0,'50px','Record Updated At',0,'string',0),
    T_SystemNavigation_alias => array(1,1,'120px','alias',0,'string',0),
    T_SystemNavigation_title => array(0,0,'100px','title',0,'string',0),
    T_SystemNavigation_description => array(1,1,'100px','description',0,'string',0),
    T_SystemNavigation_created_date => array(1,1,'100px','created_date',0,'string',0),
    T_SystemNavigation_publish => array(1,1,'100px','status',0,'string',0)

);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_SystemNavigation,
    'customfilter' => array(T_SystemNavigation_depth => 0),
    'actBTN' => "100px",
    'postBTN' => "1px",
    'tools' => array(T_SystemNavigation_RecordID,T_SystemNavigation_RecordTimestamp,T_SystemNavigation_RecordStatus,T_SystemNavigation_RecordUpdatedOn,T_SystemNavigation_RecordUpdatedBy,T_SystemNavigation_RecordUpdatedAt),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'System/navigation/Insert',
        'read' => 'webservice/read/getlist',
        'update' => 'System/navigation/Update',
        'destroy' => 'System/navigation/Delete',
        'form' => 'System/navigation/form',
        'post' => 'System/navigation/Post',
        'unpost' => 'System/navigation/UnPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>