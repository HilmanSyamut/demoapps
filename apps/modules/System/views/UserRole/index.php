<div class="row">
    <div class="col-md-12">
    <section class="panel panel-primary">
        <header class="panel-heading">
            <div class="panel-actions">
                <a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
            </div>
            <a class="btn btn-primary" href="<?php echo site_url('System/Userrole/form'); ?>">+ Add New</a>
        </header>
        <div class="panel-body">
            <table class="table table-bordered table-striped mb-none" id="datatable-default">
                <thead>
                <tr>
                    <th width="15px">Action</th>
                    <th>Role Name</th>
                </tr>
                </thead>
                <tbody>
                <?php 
                if(!empty($data)){
                    foreach ($data as $key => $item) {
                        $tool = '<a href="'.site_url("System/Userrole/form/".$item->{T_SystemUserRole_RecordID}).'"><i class="fa fa-pencil"></i></a><a href="javascript:void(0);" class="delete-row"><i class="fa fa-trash-o"></i></a>';
                        echo "<tr><td class='actions'>".$tool."</td>
                        <td><b>".$item->{T_SystemUserRole_role_name}."</b></td>
                        </tr>";
                    } 
                }
                ?>
                </tbody>
            </table>
        </div>
    </section>
    </div>
</div>