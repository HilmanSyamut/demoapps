<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
	T_SystemModules_RecordID => array(0,0,'50px','Record ID',0,'string',0),
	T_SystemModules_RecordTimestamp => array(0,0,'50px','Record Timestamp',0,'string',0),
	T_SystemModules_RecordStatus => array(0,0,'50px','Record Status',0,'string',0),
	T_SystemModules_RecordUpdatedOn => array(0,0,'50px','Record Updated On',0,'string',0),
	T_SystemModules_RecordUpdatedBy => array(0,0,'50px','Record Updated By',0,'string',0),
	T_SystemModules_RecordUpdatedAt => array(0,0,'50px','Record Updated At',0,'string',0),
	T_SystemModules_alias => array(1,1,'120px','alias',0,'string',0),
	T_SystemModules_title => array(0,0,'100px','title',0,'string',0),
	T_SystemModules_description => array(1,1,'100px','description',0,'string',0),
	T_SystemModules_icon => array(0,0,'100px','icon',0,'string',0),
	T_SystemModules_access => array(0,0,'100px','access',0,'string',0),
	T_SystemModules_author => array(1,1,'150px','author',0,'string',0),
	T_SystemModules_contributors => array(1,1,'100px','contributors',0,'string',0),
	T_SystemModules_created_date => array(1,1,'100px','created_date',0,'string',0),
	T_SystemModules_publish => array(1,1,'100px','publish',0,'string',0),
	T_SystemModules_status => array(1,1,'100px','status',0,'string',0)

);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'modul',
    'table' => T_SystemModules,
    'actBTN' => "100px",
    'postBTN' => "1px",
    'tools' => array(T_SystemModules_RecordID,T_SystemModules_RecordTimestamp,T_SystemModules_RecordStatus,T_SystemModules_RecordUpdatedOn,T_SystemModules_RecordUpdatedBy,T_SystemModules_RecordUpdatedAt),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'System/module/Insert',
        'read' => 'webservice/read/getlist',
        'update' => 'System/module/Update',
        'destroy' => 'System/module/Delete',
        'form' => 'System/module/form',
        'post' => 'System/module/Post',
        'unpost' => 'System/module/UnPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>