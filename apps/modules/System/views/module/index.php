<div class="row">
	<div class="col-md-4">
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<button class="mb-xs mt-xs mr-xs btn btn-xs btn-primary" onclick="scanAll();">scan</button>
				<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
			</div>
			<h2 class="panel-title">Modules</h2>
		</header>
		<div class="panel-body">
			<table id="modul-table">
				<thead>
				<tr>
					<th>id</th>
					<th>alias</th>
					<th>title</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($mod as $key => $modul) {
					$title = unserialize($modul->{T_SystemModules_title});
					echo "<tr><td>".$modul->{T_SystemModules_RecordID}."</td><td> ".$modul->{T_SystemModules_alias}."</td>
					<td> ".$title[$this->lang->lang()]."</td></tr>";
				} 
				?>
				</tbody>
			</table>
		</div>
	</section>
	</div>
	<div class="col-md-4">
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
				<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
			</div>
			<h2 class="panel-title">Controllers</h2>
		</header>
		<div class="panel-body">
		<table id="controller-table">
			<thead>
			<tr>
				<th>id</th>
				<th>alias</th>
				<th>title</th>
			</tr>
			</thead>
			<tbody id="tbody-controller">
			</tbody>
		</table>
		</div>
	</section>
	</div>
	<div class="col-md-4">
	<section class="panel">
		<header class="panel-heading">
			<div class="panel-actions">
				<a href="#" class="panel-action panel-action-toggle" data-panel-toggle=""></a>
				<a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss=""></a>
			</div>
			<h2 class="panel-title">Functions</h2>
		</header>
		<div class="panel-body">
			<table id="function-table">
			<thead>
			<tr>
				<th style="width: 25px;">id</th>
				<th>alias</th>
				<th>title</th>
			</tr>
			</thead>
			<tbody id="tbody-function">
			</tbody>
		</table>
		</div>
	</section>
	</div>
</div>
<script>
    $(document).ready(function() {
        $("#modul-table").kendoGrid({
            height: 400,
            sortable: true,
            selectable: true,
            change: onChange
        });

         $("#controller-table").kendoGrid({
            height: 400,
            sortable: true,
            selectable: true,
            change: onFunctionx
        });

         $("#function-table").kendoGrid({
            height: 400,
            sortable: true,
            selectable: true
        });
    });

    function scanAll()
    {
    	var voData = {
			scan: true,
	    };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('Welcomes/getModule'),
            success: function (result) {
            	// window.location.replace(current_url());
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
    }

    function onChange(arg) {
        var selected = $.map(this.select(), function(item) {
            return $(item).text();
        });
        selected = selected[0].split(" ");
        getController(selected[0]);
    }

    function onFunctionx(arg) {
        var selected = $.map(this.select(), function(item) {
            return $(item).text();
        });
        selected = selected[0].split(" ");
        getFunctionx(selected[0]);
    }

    function getController(id)
	{
	    var voData = {
			ID: id,
	    };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('System/Module/getController'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                	$("#tbody-controller").html(result.html);
                	$("#tbody-function").html("");
                	if(!$("#controller-table").data("kendoGrid")){
				        $("#controller-table").kendoGrid({
				            height: 350,
				            sortable: true,
				            selectable: true,
				            change: onFunctionx
				        });
				    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                new PNotify({ title: errorThrown, text: errorThrown, type: 'error', shadow: true });
            }
        });
	}

	function getFunctionx(id)
	{
	    var voData = {
			ID: id,
	    };
        $.ajax({
            type: 'GET',
            data: voData,
            url:  site_url('System/module/getFunctionx'),
            success: function (result) {
                if (result.errorcode > 0) {
                    new PNotify({ title: "Failed", text: result.msg, type: 'error', shadow: true });
                } else {
                	$("#tbody-function").html(result.html);
                	if(!$("#function-table").data("kendoGrid")){
				        $("#function-table").kendoGrid({
				            height: 350,
				            sortable: true,
				            selectable: true
				        });
				    }
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(jQuery.parseJSON(jqXHR.responseText));
            }
        });
	}
</script>