<?php 
// Column name => |form|grid|width|title|editable|typeData|required|
$column = array(
	T_SystemControllers_RecordID => array(0,0,'50px','Record ID',0,'string',0),
	T_SystemControllers_RecordTimestamp => array(0,0,'50px','Record Timestamp',0,'string',0),
	T_SystemControllers_RecordStatus => array(0,0,'50px','Record Status',0,'string',0),
	T_SystemControllers_RecordUpdatedOn => array(0,0,'50px','Record Updated On',0,'string',0),
	T_SystemControllers_RecordUpdatedBy => array(0,0,'50px','Record Updated By',0,'string',0),
	T_SystemControllers_RecordUpdatedAt => array(0,0,'50px','Record Updated At',0,'string',0),
	T_SystemControllers_alias => array(1,1,'120px','alias',0,'string',0),
	T_SystemControllers_title => array(0,0,'100px','title',0,'string',0),
	T_SystemControllers_description => array(1,1,'100px','description',0,'string',0),
	T_SystemControllers_author => array(1,1,'150px','author',0,'string',0),
	T_SystemControllers_contributors => array(1,1,'100px','contributors',0,'string',0),
	T_SystemControllers_created_date => array(1,1,'100px','created_date',0,'string',0),
	T_SystemControllers_status => array(1,1,'100px','status',0,'string',0)

);
// Column DropdownList => |Text|URL|
// $dropdownlist = array(
//     'GroupCode' => array('GroupName','master/GroupListGet')
// );
// variable attribute for gridview
$attr = array(
    'id'=>'grid',
    'table' => T_SystemControllers,
    'actBTN' => "100px",
    'postBTN' => "1px",
    'tools' => array(T_SystemControllers_RecordID,T_SystemControllers_RecordTimestamp,T_SystemControllers_RecordStatus,T_SystemControllers_RecordUpdatedOn,T_SystemControllers_RecordUpdatedBy,T_SystemControllers_RecordUpdatedAt),
    'column' => $column,
    // 'dropdownlist' => $dropdownlist,
    'url' => array(
        'create' => 'System/controller/Insert',
        'read' => 'webservice/read/getlist',
        'update' => 'System/controller/Update',
        'destroy' => 'System/controller/Delete',
        'form' => 'System/controller/form',
        'post' => 'System/controller/Post',
        'unpost' => 'System/controller/UnPost'
    )
);
// generate gridView
echo onlyGridView($attr); 
?>