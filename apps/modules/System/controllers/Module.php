<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * Modules Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Module extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Module_model');
	}

	public function index()
	{
		$data['mod'] = $this->Module_model->getData();
		$this->modules->render('/module/index',$data);
	}

	public function getController()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$id = $_GET["ID"];
		$data = $this->Module_model->getController($id);
		$html = "";
		if(count($data) > 0){
			foreach ($data as $key => $value) {
				$title = unserialize($value->{T_SystemControllers_title});
				$html .= "<tr><td>".$value->{T_SystemControllers_RecordID}."</td>
				<td>".$value->{T_SystemControllers_alias}."</td>
				<td>".$title[$this->lang->lang()]."</td>
				</tr>";
			}
			$info->html = $html;
		}else{
		$info->errorcode = 300;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function getFunctionx()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;
		$id = $_GET["ID"];
		$data = $this->Module_model->getFunctionx($id);
		$html = "";
		if(count($data) > 0){
			foreach ($data as $key => $value) {
				$html .= "<tr><td>".$value->{T_SystemMethode_RecordID}."</td>
				<td>&nbsp;".$value->{T_SystemMethode_alias}."</td>
				<td>&nbsp;".$value->{T_SystemMethode_title}."</td>
				</tr>";
			}
			$info->html = $html;
		}else{
		$info->errorcode = 300;
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function form($id='')
	{
		$data = $this->Modules_model->getDetail($id);
		$this->modules->render('/module/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Modules_model->getDetail($id);
		$this->modules->render('/module/formDetail', $data);
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_SystemModules_alias   => $this->input->post("alias"),
					T_SystemModules_title   => $this->input->post("title"),
					T_SystemModules_description   => $this->input->post("description"),
					T_SystemModules_icon   => $this->input->post("icon"),
					T_SystemModules_access   => $this->input->post("access"),
					T_SystemModules_author   => $this->input->post("author"),
					T_SystemModules_contributors   => $this->input->post("contributors"),
					T_SystemModules_created_date   => $this->input->post("created_date"),
					T_SystemModules_publish   => $this->input->post("publish"),
					T_SystemModules_status   => $this->input->post("status")
				);
				$this->Modules_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemModules_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SystemModules_RecordID   => $this->input->post("RecordID"),
					T_SystemModules_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemModules_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemModules_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemModules_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemModules_alias   => $this->input->post("alias"),
					T_SystemModules_title   => $this->input->post("title"),
					T_SystemModules_description   => $this->input->post("description"),
					T_SystemModules_icon   => $this->input->post("icon"),
					T_SystemModules_access   => $this->input->post("access"),
					T_SystemModules_author   => $this->input->post("author"),
					T_SystemModules_contributors   => $this->input->post("contributors"),
					T_SystemModules_created_date   => $this->input->post("created_date"),
					T_SystemModules_publish   => $this->input->post("publish"),
					T_SystemModules_status   => $this->input->post("status")
					);

				$this->Modules_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Modules_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Modules.php */
/* Location: ./app/modules/System/controllers/Modules.php */
