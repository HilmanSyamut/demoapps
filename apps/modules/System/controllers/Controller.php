<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * Controllers Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Controller extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		// $this->load->model('Controllers_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/Controller/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Controllers_model->getDetail($id);
		$this->modules->render('/Controller/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Controllers_model->getDetail($id);
		$this->modules->render('/Controller/formDetail', $data);
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_SystemControllers_RecordTimestamp 	=> date("Y-m-d g:i:s",now()),
					T_SystemControllers_id   => $this->input->post("id"),
					T_SystemControllers_module_id   => $this->input->post("module_id"),
					T_SystemControllers_alias   => $this->input->post("alias"),
					T_SystemControllers_title   => $this->input->post("title"),
					T_SystemControllers_description   => $this->input->post("description"),
					T_SystemControllers_author   => $this->input->post("author"),
					T_SystemControllers_contributor   => $this->input->post("contributor"),
					T_SystemControllers_created_date   => $this->input->post("created_date"),
					T_SystemControllers_status   => $this->input->post("status"),

					'detail'			=> $this->input->post("detail"),
				);
				$this->Controllers_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemControllers_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SystemControllers_RecordID   => $this->input->post("RecordID"),
					T_SystemControllers_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemControllers_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemControllers_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemControllers_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemControllers_id   => $this->input->post("id"),
					T_SystemControllers_module_id   => $this->input->post("module_id"),
					T_SystemControllers_alias   => $this->input->post("alias"),
					T_SystemControllers_title   => $this->input->post("title"),
					T_SystemControllers_description   => $this->input->post("description"),
					T_SystemControllers_author   => $this->input->post("author"),
					T_SystemControllers_contributor   => $this->input->post("contributor"),
					T_SystemControllers_created_date   => $this->input->post("created_date"),
					T_SystemControllers_status   => $this->input->post("status"),

					'detail' => $this->input->post("detail"), 
					);

				$this->Controllers_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Controllers_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Controllers.php */
/* Location: ./app/modules/System/controllers/Controllers.php */
