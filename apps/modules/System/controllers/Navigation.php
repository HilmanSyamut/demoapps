<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * Navigation Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Navigation extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('Navigations_model');
	}

	public function index()
	{
		$data['data'] = $this->Navigations_model->getData();
		$this->modules->render('/navigation/index',$data);
	}

	public function form($id='')
	{
		$data = $this->Navigations_model->getDetail($id);
		$this->modules->render('/navigation/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->Navigations_model->getDetail($id);
		$this->modules->render('/navigation/formDetail', $data);
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$depth = ($this->input->post("subParent")) ? $this->input->post("subParent") : $this->input->post("depth");
				$data = array(
					T_SystemNavigation_alias   => $this->input->post("alias"),
					T_SystemNavigation_title   => serialize($this->input->post("title")),
					T_SystemNavigation_description   => $this->input->post("description"),
					T_SystemNavigation_icon   => $this->input->post("icon"),
					T_SystemNavigation_depth   => ($depth) ? $depth : 0,
					T_SystemNavigation_ordering   => $this->input->post("ordering"),
					T_SystemNavigation_publish   => $this->input->post("publish"),
					T_SystemNavigation_type   => $this->input->post("type")
				);
				$this->db->Insert(T_SystemNavigation,$data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemNavigation_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$depth = ($this->input->post("subParent")) ? $this->input->post("subParent") : $this->input->post("depth");
				$data = array(
					T_SystemNavigation_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemNavigation_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemNavigation_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemNavigation_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemNavigation_alias   => $this->input->post("alias"),
					T_SystemNavigation_title   => serialize($this->input->post("title")),
					T_SystemNavigation_description   => $this->input->post("description"),
					T_SystemNavigation_icon   => $this->input->post("icon"),
					T_SystemNavigation_depth   => ($depth) ? $depth : 0,
					T_SystemNavigation_ordering   => $this->input->post("ordering"),
					T_SystemNavigation_publish   => $this->input->post("publish"),
					T_SystemNavigation_type   => $this->input->post("type")
				);
				$this->db->where(T_SystemNavigation_RecordID,$this->input->post("RecordID"));
				$this->db->update(T_SystemNavigation,$data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->Navigations_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function set_cat()
	{
		$this->load->model('options_model');
		$form = '';
		if($_POST['id'] == 2 || $_POST['id'] == 3):
		$form .= '<br>';
		$form .= '<div class="row">';
		$form .= '<label class="col-md-3 form-label">Parent</label>';
		$form .= '<div class="col-md-5">';
		$form .= form_dropdown('depth', $this->options_model->get_lookup('0',T_SystemNavigation,T_SystemNavigation_depth,T_SystemNavigation_alias,T_SystemNavigation_RecordID), (isset($depth) ? $depth : '') ,'class="form-control select2" id="depth" onChange="setsubParent(this.value);" ');
		$form .= '</div>';
		$form .= '</div>';
		endif;
		$output = array('errorcode' => 0, 'msg' => 'success', 'form' => $form);
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function set_sub()
	{
		$this->load->model('options_model');
		$form = '';
		if($_POST['type'] == 3):
		$form .= '<br>';
		$form .= '<div class="row">';
		$form .= '<label class="col-md-3 form-label">Sub Parent</label>';
		$form .= '<div class="col-md-5">';
		$form .= form_dropdown('subParentf', $this->options_model->get_lookup($_POST['id'],T_SystemNavigation,T_SystemNavigation_depth,T_SystemNavigation_alias,T_SystemNavigation_RecordID), (isset($depth) ? $depth : '') ,'class="form-control select2" id="subParentf"');
		$form .= '</div>';
		$form .= '</div>';
		endif;
		$output = array('errorcode' => 0, 'msg' => 'success', 'form' => $form);
        $this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file Navigation.php */
/* Location: ./app/modules/System/controllers/Navigation.php */
