<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * UserRole Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Userrole extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('UserRole_model');
		$this->load->model('User_model');
	}

	public function index()
	{
		$data['data'] = $this->UserRole_model->getData();
		$this->modules->render('/UserRole/index',$data);
	}

	public function form($id='')
	{
		$data = $this->UserRole_model->getDetail($id);
		$data['module'] = $this->User_model->getListModule();
		$data['menu'] = $this->User_model->getListMenu();
		$this->modules->render('/UserRole/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->UserRole_model->getDetail($id);
		$this->modules->render('/UserRole/formDetail', $data);
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				if(!empty($this->input->post("access"))){
					$access = explode(",",$this->input->post('access'));
					$access = array_filter($access);
					$access = implode(',', $access);
					$access = array_map('intval', explode(',', $access));
					$access = serialize($access);
				}else{
					$access = 'a:1:{i:0;s:1:"1";}';
				}
				if(!empty($this->input->post("acceesMenu"))){
					$accessMenu = explode(",",$this->input->post('acceesMenu'));
					$accessMenu = array_filter($accessMenu);
					$accessMenu = implode(',', $accessMenu);
					$accessMenu = array_map('intval', explode(',', $accessMenu));
					$accessMenu = serialize($accessMenu);
				}else{
					$accessMenu = 'a:1:{i:0;s:1:"1";}';
				}
				$data = array(
					T_SystemUserRole_role_name   => $this->input->post("RoleName"),
					T_SystemUserRole_default_access   => $accessMenu,
					T_SystemUserRole_access   => $access
				);
				$this->UserRole_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemUserRole_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				if(!empty($this->input->post("access"))){
					$access = explode(",",$this->input->post('access'));
					$access = array_filter($access);
					$access = implode(',', $access);
					$access = array_map('intval', explode(',', $access));
					$access = serialize($access);
				}else{
					$access = 'a:1:{i:0;s:1:"1";}';
				}
				if(!empty($this->input->post("acceesMenu"))){
					$accessMenu = explode(",",$this->input->post('acceesMenu'));
					$accessMenu = array_filter($accessMenu);
					$accessMenu = implode(',', $accessMenu);
					$accessMenu = array_map('intval', explode(',', $accessMenu));
					$accessMenu = serialize($accessMenu);
				}else{
					$accessMenu = 'a:1:{i:0;s:1:"1";}';
				}
				$data = array(
					T_SystemUserRole_RecordID   => $this->input->post("RecordID"),
					T_SystemUserRole_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemUserRole_RecordUpdatedOn  => now(),
					T_SystemUserRole_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemUserRole_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemUserRole_role_name   => $this->input->post("RoleName"),
					T_SystemUserRole_default_access   => $accessMenu,
					T_SystemUserRole_access   => $access
				);

				$this->UserRole_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->UserRole_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file UserRole.php */
/* Location: ./app/modules/System/controllers/UserRole.php */
