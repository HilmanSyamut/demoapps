<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Hikmat Fauzy
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Modules
 *
 * UserMeta Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 22.08.2016	
 * @author	    Hikmat Fauzy
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Usermeta extends BC_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('UserMeta_model');
	}

	public function index()
	{
		$data = '';
		$this->modules->render('/UserMeta/index',$data);
	}

	public function form($id='')
	{
		$data = $this->UserMeta_model->getDetail($id);
		$this->modules->render('/UserMeta/form', $data);
	}

	public function formDetail($id='')
	{
		$data = $this->UserMeta_model->getDetail($id);
		$this->modules->render('/UserMeta/formDetail', $data);
	}

	public function Insert(){
		try{
			if (FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Data dengan index '.$docNo.' sudah ada');
			}else{
				$data = array(
					T_SystemUserMeta_RecordTimestamp 	=> date("Y-m-d g:i:s",now()),
					T_SystemUserMeta_user_id   => $this->input->post("user_id"),
					T_SystemUserMeta_first_name   => $this->input->post("first_name"),
					T_SystemUserMeta_last_name   => $this->input->post("last_name"),
					T_SystemUserMeta_phone   => $this->input->post("phone"),
					T_SystemUserMeta_photo   => $this->input->post("photo"),
					T_SystemUserMeta_address   => $this->input->post("address"),
					T_SystemUserMeta_provinsi_id   => $this->input->post("provinsi_id"),
					T_SystemUserMeta_kecamatan_id   => $this->input->post("kecamatan_id"),
					T_SystemUserMeta_kabkot_id   => $this->input->post("kabkot_id"),
					T_SystemUserMeta_facebook   => $this->input->post("facebook"),
					T_SystemUserMeta_twitter   => $this->input->post("twitter"),
					T_SystemUserMeta_website   => $this->input->post("website"),
					T_SystemUserMeta_office   => $this->input->post("office"),

					'detail'			=> $this->input->post("detail"),
				);
				$this->UserMeta_model->Insert($data);

				$id = $this->db->insert_id();
				$activity_log = array(
					'msg'=> 'Insert new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $id
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Update(){
		try{
			if(check_column(T_SystemUserMeta_RecordTimestamp, 'RecordTimestamp') == FALSE)
			{
				$output = array('errorcode' => 200, 'msg' => 'Failed to update data, another user has been update this data');
			}else{
				$data = array(
					T_SystemUserMeta_RecordID   => $this->input->post("RecordID"),
					T_SystemUserMeta_RecordStatus   => $this->input->post("RecordStatus"),
					T_SystemUserMeta_RecordUpdatedOn  => date('Y-m-d g:i:s',now()),
					T_SystemUserMeta_RecordUpdatedBy  => $this->ezrbac->getCurrentUserID(),
					T_SystemUserMeta_RecordUpdatedAt  => $this->input->ip_address(),
					T_SystemUserMeta_user_id   => $this->input->post("user_id"),
					T_SystemUserMeta_first_name   => $this->input->post("first_name"),
					T_SystemUserMeta_last_name   => $this->input->post("last_name"),
					T_SystemUserMeta_phone   => $this->input->post("phone"),
					T_SystemUserMeta_photo   => $this->input->post("photo"),
					T_SystemUserMeta_address   => $this->input->post("address"),
					T_SystemUserMeta_provinsi_id   => $this->input->post("provinsi_id"),
					T_SystemUserMeta_kecamatan_id   => $this->input->post("kecamatan_id"),
					T_SystemUserMeta_kabkot_id   => $this->input->post("kabkot_id"),
					T_SystemUserMeta_facebook   => $this->input->post("facebook"),
					T_SystemUserMeta_twitter   => $this->input->post("twitter"),
					T_SystemUserMeta_website   => $this->input->post("website"),
					T_SystemUserMeta_office   => $this->input->post("office"),

					'detail' => $this->input->post("detail"), 
					);

				$this->UserMeta_model->Update($data);
				
				$activity_log = array(
					'msg'=> 'Update new list',
					'kategori'=> 7,
					'jenis'=> 1,
					'object'=> $this->input->post("RecordID")
				);
				activity_log($activity_log);

				$output = array('errorcode' => 0, 'msg' => 'success');
			}
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Delete()
	{
		try{
			$this->UserMeta_model->Delete($this->input->post("RecordID"));
			
			$activity_log = array(
				'msg'=> 'Delete list',
				'kategori'=> 7,
				'jenis'=> 3,
				'object'=> $this->input->post("RecordID")
			);
			activity_log($activity_log);

			$output = array('errorcode' => 0, 'msg' => 'success');
		}catch(Exception $e)
		{
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function Post(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

			$output = array('errorcode' => 0, 'msg' => 'success');
		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}

	public function UnPost(){
		try {
			$info = new stdClass();
			$info->msg = "";
			$info->errorcode = 0;
			
			// Your Logic Here

		} catch (Exception $e) {
			$output = array('errorcode' => 100, 'msg' => $e->getMessage());
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($output));
	}
}

/* End of file UserMeta.php */
/* Location: ./app/modules/System/controllers/UserMeta.php */
