<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * MasterData Navigations
 *
 * Navigation Model
 *
 * @package     App
 * @subpackage  Navigations
 * @category    Navigation Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Navigations_model extends CI_Model
{    

    public function getData(){
    	$this->db->where(T_SystemNavigation_depth,0);
        $query = $this->db->get(T_SystemNavigation);
        return $query->result();
    }

    public function getDataChild($id){
    	$this->db->where(T_SystemNavigation_depth,$id);
        $query = $this->db->get(T_SystemNavigation);
        return $query->result();
    }

    public function getDetail($id){
        $this->db->where(T_SystemNavigation_RecordID,$id);
        $query = $this->db->get(T_SystemNavigation);
        $data = $query->first_row('array');
        if(!empty($id)){
            $data['Detail'] = $this->getDetailItem($data[T_SystemNavigation_RecordID]);
        }
        return $data;
    }

    public function getDetailItem($id)
    {
        $this->db->where(T_SystemNavigation_depth,$id);
        $this->db->order_by(T_SystemNavigation_ordering);
        $query = $this->db->get(T_SystemNavigation);
        $data = $query->result("array");
        return $data;
    }

    public function Delete($id){
        $this->db->trans_begin();
        $this->db->delete(T_SystemNavigation, array(T_SystemNavigation_RecordID => $id));
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}
