<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System User Role
 *
 * User Role Model
 *
 * @package     App
 * @subpackage  User
 * @category    User Role Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Userrole_model extends CI_Model
{    

    public function getData(){
        $query = $this->db->get(T_SystemUserRole);
        return $query->result();
    }

    public function getDetail($id){
        $this->db->where(T_SystemUserRole_RecordID,$id);
        $query = $this->db->get(T_SystemUserRole);
        $data = $query->first_row('array');
        return $data;
    }

    public function insert($data)
    {
        $this->db->trans_begin();
        $this->db->insert(T_SystemUserRole,$data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function update($data)
    {
        $this->db->trans_begin();
        $ID = $data[T_SystemUserRole_RecordID];
        unset($data[T_SystemUserRole_RecordID]);
        $this->db->where(T_SystemUserRole_RecordID,$ID);
        $this->db->update(T_SystemUserRole,$data);

        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}
