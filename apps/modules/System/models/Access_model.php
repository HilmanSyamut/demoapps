<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package     BluesCode
 * @author      Hikmat Fauzy
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
+
 * System Access
 *
 * Access Model
 *
 * @package     App
 * @subpackage  User
 * @category    Access Model
 * 
 * @version     1.1 Build 22.08.2016    
 * @author      Hikmat Fauzy
 * @contributor 
 * @copyright   Copyright (c) 2013 - 2017, Global Trend Asia
 * @license     http://www.cplus-studio.net/bluescode/license.html
 * @link        http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Access_model extends CI_Model
{    

    public function getData(){
        return true;
    }
}
