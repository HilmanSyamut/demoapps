<div class="col-sm-12">
 <span class="text-right">
    <button id="IntervalStart" class="mb-xs mt-xs mr-xs btn btn-xs btn-info" onclick="myInterval(1)" style="display:none;">Start</button>
    <button id="IntervalStop" class="mb-xs mt-xs mr-xs btn btn-xs btn-warning" onclick="myInterval(0)">Stop</button>
    <button id="alertreset" class="mb-xs mt-xs mr-xs btn btn-xs btn-danger" onclick="alertreset(0)">Reset</button>
</span>
</div>
<audio id="myAudio" src="assets\backend\audio\red_alert.wav" preload="auto"></audio>
<div class="col-sm-12" id="gridalertview" style="display: none">
    <section class="panel panel-secondary">
        <header id="btn-blink" class="panel-heading">
            <div class="panel-actions">
                <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
            </div>
            <h2 class="panel-title">Alert</h2>
        </header>
        <header id="btn-noblink" class="panel-heading">
            <div class="panel-actions">
                <a data-panel-toggle="" class="panel-action panel-action-toggle" href="#"></a>
            </div>
            <h2 class="panel-title">Alert</h2>
        </header>
        <div class="panel-body">

        <div id="gridAlert"></div>
        <script type="text/javascript">
        $(document).ready(function() {
        //Grid
            $("#gridAlert").kendoGrid({  
                height: "330px",
                width: "100%",
                columns: [
                {
                "title":"ItemID",
                "width":"50px",
                "field":"<?php echo T_MasterDataItem_ItemID; ?>",
                },
                {
                "title":"Item Name",
                "width":"100px",
                "field":"<?php echo T_MasterDataItem_ItemName; ?>",
                }
                ],                     
                dataSource: {
                    transport: {
                        read: {
                            type:"GET",
                            data: { table: 't1022'},
                            url: site_url('Dashboard/alertData'),
                            dataType: "json"
                        }
                    },
                    sync: function(e) {
                        $('#gridAlert').data('kendoGrid').dataSource.read();
                        $('#gridAlert').data('kendoGrid').refresh();
                    },
                    schema: {
                        data: function(data){
                            return data.data;
                        },
                        total: function(data){
                            return data.count;
                        },
                        model: {
                            id: "t1010r001",
                        }
                    },
                    pageSize: 10,
                    serverPaging: true,
                    serverFiltering: true,
                    serverSorting: true
                },        
                filterable: false,
                selectable: true,
                autoBind:false,       
                groupable: false,
                sortable: true,
                resizable: true,
                scrollable: true,
                reorderable:true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
            });
        });
    </script>
    </div>
</section>
</div>

<script type="text/javascript">

var realTime = setInterval(function(){ getData(); }, 1000);

function getData()
{
    $.ajax({
    type: 'POST',
    url: site_url('Dashboard/alertCount'),
    dataType: "json",
        success: function (data) {
        $("#alert").text(data.Qty);
        viewalert();
        if(parseFloat(data.Qty) > 0){
            $("#btn-blink").show();
            $("#btn-noblink").hide();
            myAudio.play();
        }else{
            $("#btn-blink").hide();
            $("#btn-noblink").show();
            myAudio.pause();
        }
    },
    error: function (jqXHR, textStatus, errorThrown) {
        alert(jQuery.parseJSON(jqXHR.responseText));
    }
    });
}
function viewalert(){
    $('#gridAlert').data('kendoGrid').dataSource.read();
    $('#gridAlert').data('kendoGrid').refresh();
    $("#gridalertview").removeAttr('style');
}
function myInterval(i) {
    if(i){
        $("#IntervalStart").hide();
        $("#IntervalStop").show();
        realTime = setInterval(function(){ getData(); }, 1000);
    }else{
        $("#IntervalStart").show();
        $("#IntervalStop").hide();
        clearInterval(realTime);
    }
}

function alertreset()
{
    $.ajax({
    type: 'POST',
    url: site_url('Dashboard/resetAlert'),
    dataType: "json",
        success: function (data) {
            $("#btn-blink").hide();
            $("#btn-noblink").show();
        },
    error: function (jqXHR, textStatus, errorThrown) {
        alert(jQuery.parseJSON(jqXHR.responseText));
    }
    });
}

var myAudio = document.getElementById("myAudio");
myAudio.pause();

</script>
<style>
/*#btn-blink {
  background-color: #737373;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  border: none;
  color: #FFFFFF;
  cursor: pointer;
  display: inline-block;
  font-family: Arial;
  font-size: 20px;
  padding: 50px 50px;
  text-align: center;
  text-decoration: none;
  margin-bottom:-6px;
}
#btn-noblink {
  background-color: #737373;
  -webkit-border-radius: 50px;
  border-radius: 50px;
  border: none;
  color: #FFFFFF;
  cursor: pointer;
  display: inline-block;
  font-family: Arial;
  font-size: 20px;
  padding: 50px 50px;
  text-align: center;
  text-decoration: none;
  margin-bottom:-6px;
}*/
@-webkit-keyframes glowing {
  0% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -webkit-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -webkit-box-shadow: 0 0 3px #B20000; }
}

@-moz-keyframes glowing {
  0% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; -moz-box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; -moz-box-shadow: 0 0 3px #B20000; }
}

@-o-keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}

@keyframes glowing {
  0% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
  50% { background-color: #FF0000; box-shadow: 0 0 40px #FF0000; }
  100% { background-color: #B20000; box-shadow: 0 0 3px #B20000; }
}

#btn-blink {
  -webkit-animation: glowing 500ms infinite;
  -moz-animation: glowing 500ms infinite;
  -o-animation: glowing 500ms infinite;
  animation: glowing 500ms infinite;
}
</style>