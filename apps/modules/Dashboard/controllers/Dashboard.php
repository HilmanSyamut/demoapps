<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * BluesCode CMS
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2016, Global trend asia PT.
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Component Dashboard
 *
 * Dashboard Controller
 *
 * @package	    App
 * @subpackage	Modules
 * @category	Module Controller
 * 
 * @version     1.1 Build 14.12.2016	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2016, Global trend asia PT.
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
class Dashboard extends BC_Controller 
{
	
	function __construct()
    {
    	parent::__construct();
		$this->load->model(array('Dashboard_model'));
	}

	public function inout()
	{
		$this->template->set_layout('menucollapse');
		$data = '';
		$this->modules->render('index', $data);
	}

	public function alert($id=0)
	{
		$view = ($id) ? 'alert' : "alertnew";
		$this->template->set_layout('menucollapse');
		$data = '';
		$this->modules->render($view, $data);
	}

	public function stockOnhand()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockBalanceHeader;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getList($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockSold()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "v1991";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1991r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListstockSold($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListstockSoldCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockIn()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = "v1991";
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = 't1991r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListstockIn($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListstockInCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function alertData()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_VIEWAlert;
		$limit = $this->input->get('pageSize');
		$offset = $this->input->get('skip');
		if($this->input->get('sort')){
			$sort = $this->input->get('sort');
			$sortfield = $sort[0]['field'];
			$sortdir = $sort[0]['dir'];
		}else{
			$sortfield = $table.'r001';
   			$sortdir = 'DESC';
		}
		if ($this->input->get('filter')) {
			$filter = $this->input->get('filter');
			if (isset($filter['filters'][0]['ignoreCase'])) {
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = TRUE;
			}else{
				$searchOperator = $filter['filters'][0]['operator'];
				$searchValue = $filter['filters'][0]['value'];
				$searchField = $filter['filters'][0]['field'];
				$searchignoreCase = FALSE;
			}
		}else{
			$searchOperator = '';
			$searchValue = '';
			$searchField = '';
			$searchignoreCase = '';
		}
		$getList = $this->Dashboard_model->getListAlert($table, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField);
	
		if($getList->num_rows() > 0){
			$info->data = $getList->result();
			$info->count = $this->Dashboard_model->getListAlertCount($table);
		}else{
			$info->errorcode = 32;
			$info->msg = "Data Tidak Di Temukan";
		}
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function resetAlert()
	{
		$this->db->update(T_Alert,array(T_Alert_FLAG => 1));
	}

	public function stockOnhandCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$table = T_TransactionStockBalanceHeader;
		$this->db->select('SUM('.T_TransactionStockBalanceHeader_Quantity.') as QtyIn');
		$sql = $this->db->get($table);
		$info->Qty = ($sql->first_row()->QtyIn) ? $sql->first_row()->QtyIn : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockSoldCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		//$query = "Select SUM(t1011f010) as QtyOut from t1010 JOIN t1011 on t1011.t1011f001 = t1010.t1010r001 where t1010f001 in ('IVSO') AND t1010r003 = 1";
		$query = "SELECT SUM(t1991f005) AS QtyOut FROM v1991 WHERE t1991f002 IN ('IVSO','SLPS','IVST')";
		$sql = $this->db->query($query);
		$info->Qty = ($sql->first_row()->QtyOut) ? $sql->first_row()->QtyOut : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function alertCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$query = "Select t7011f003 as Qty, t8010f001 as ItemID from v7011 WHERE	DATE_FORMAT(t7011r002, '%Y-%m-%d') = '".date("Y-m-d")."' AND t7011f005 = 0";
		$sql = $this->db->query($query);
		$info->Qty = empty($sql->first_row()->ItemID) ? 0 : $sql->first_row()->Qty;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}

	public function stockInCount()
	{
		$info = new stdClass();
		$info->msg = "";
		$info->errorcode = 0;

		$query = "SELECT	SUM(t1011f010) AS QtyIn FROM t1011 JOIN t1010 ON t1010.t1010r001 = t1011f001 WHERE t1010.t1010f001 IN ('IMBB', 'IVSI') and t1010.t1010r003 = 1";
		$sql = $this->db->query($query);
		$info->Qty = ($sql->first_row()->QtyIn) ? $sql->first_row()->QtyIn : 0;
		$this->output->set_content_type('application/json')->set_output(json_encode($info));
	}
}

/* End of file dashboard.php */
/* Location: ./app/modules/transaction/controllers/dashboard.php */