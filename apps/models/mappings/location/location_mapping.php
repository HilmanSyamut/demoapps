<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('T_LOCATION')   OR define('T_LOCATION', 't8030');
defined('T_LOCATION_RecordID')   OR define('T_LOCATION_RecordID', 't8030r001');
defined('T_LOCATION_RecordTimeStamp')   OR define('T_LOCATION_RecordTimeStamp', 't8030r002');
defined('T_LOCATION_RecordStatus')   OR define('T_LOCATION_RecordStatus', 't8030r003');
defined('T_LOCATION_UpdateAt')   OR define('T_LOCATION_UpdateAt', 't8030r004');
defined('T_LOCATION_UpdateBy')   OR define('T_LOCATION_UpdateBy', 't8030r005');
defined('T_LOCATION_UpdateOn')   OR define('T_LOCATION_UpdateOn', 't8030r006');
defined('T_LOCATION_ID')   OR define('T_LOCATION_ID', 't8030f001');
defined('T_LOCATION_Name')   OR define('T_LOCATION_Name', 't8030f002');

