<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('T_SBHeader')   OR define('T_SBHeader', 't1010');
defined('T_SBHeader_RecordID')   OR define('T_SBHeader_RecordID', 't1010r001');
defined('T_SBHeader_RecordTimeStamp')   OR define('T_SBHeader_RecordTimeStamp', 't1010r002');
defined('T_SBHeader_RecordStatus')   OR define('T_SBHeader_RecordStatus', 't1010r003');
defined('T_SBHeader_UpdateAt')   OR define('T_SBHeader_UpdateAt', 't1010r004');
defined('T_SBHeader_UpdateBy')   OR define('T_SBHeader_UpdateBy', 't1010r005');
defined('T_SBHeader_UpdateOn')   OR define('T_SBHeader_UpdateOn', 't1010r006');
defined('T_SBHeader_DocTypeID')   OR define('T_SBHeader_DocTypeID', 't1010f001');
defined('T_SBHeader_DocNo')   OR define('T_SBHeader_DocNo', 't1010f002');
defined('T_SBHeader_DocDate')   OR define('T_SBHeader_DocDate', 't1010f003');
defined('T_SBHeader_DocStatus')   OR define('T_SBHeader_DocStatus', 't1010f004');
defined('T_SBHeader_Remarks')   OR define('T_SBHeader_Remarks', 't1010f005');

defined('T_SBDetail')   OR define('T_SBDetail', 't1011');
defined('T_SBDetail_RecordID')   OR define('T_SBDetail_RecordID', 't1011r001');
defined('T_SBDetail_RecordTimeStamp')   OR define('T_SBDetail_RecordTimeStamp', 't1011r002');
defined('T_SBDetail_RecordStatus')   OR define('T_SBDetail_RecordStatus', 't1011r003');
defined('T_SBDetail_UpdateAt')   OR define('T_SBDetail_UpdateAt', 't1011r004');
defined('T_SBDetail_UpdateBy')   OR define('T_SBDetail_UpdateBy', 't1011r005');
defined('T_SBDetail_UpdateOn')   OR define('T_SBDetail_UpdateOn', 't1011r006');
defined('T_SBDetail_ParentID')   OR define('T_SBDetail_ParentID', 't1011f001');
defined('T_SBDetail_RowIndex')   OR define('T_SBDetail_RowIndex', 't1011f002');
defined('T_SBDetail_ItemID')   OR define('T_SBDetail_ItemID', 't1011f003');
defined('T_SBDetail_ItemDescription')   OR define('T_SBDetail_ItemDescription', 't1011f004');
defined('T_SBDetail_EPC')   OR define('T_SBDetail_EPC', 't1011f005');
defined('T_SBDetail_Barcode')   OR define('T_SBDetail_Barcode', 't1011f006');
defined('T_SBDetail_Location1')   OR define('T_SBDetail_Location1', 't1011f007');
defined('T_SBDetail_Location2')   OR define('T_SBDetail_Location2', 't1011f008');
defined('T_SBDetail_Qty1')   OR define('T_SBDetail_Qty1', 't1011f009');
defined('T_SBDetail_Qty2')   OR define('T_SBDetail_Qty2', 't1011f010');
defined('T_SBDetail_Remarks')   OR define('T_SBDetail_Remarks', 't1011f011');
