<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Hospital Information System
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Apotek Farmasi Modules
 *
 * Ajax Model
 *
 * @package	    Site
 * @subpackage	Components
 * @category	Components Model
 * 
 * @version     1.1 Build 02.09.2014	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------

class Ajax_Model extends CI_Model
{
    
    public function get_norm()
    {
    	$query = $this->db->get('pasien');
    	$result = $query->result();
    	return $result;
    }
    public function get_data_pasien($norm)
    {
    	$this->db->where('no_rekam_medik',$norm);
    	$query = $this->db->get('pasien');
    	$result = $query->result();
    	$result = (!empty($result) ? $result[0] : '');
    	return $result;
    }
    public function get_tindakan($id)
    {
    	$this->db->where('id',$id);
    	$query = $this->db->get('view_tindakan_ruangan');
    	$result = $query->result();
    	return $result;
    }
    public function get_paketbhp($id)
    {
    	$this->db->where('id',$id);
    	$query = $this->db->get('view_paket_tindakan_alkes');
    	$result = $query->result();
    	return $result;
    }
    public function get_alkesbhp($id)
    {
    	$this->db->where('id',$id);
    	$query = $this->db->get('farmasi');
    	$result = $query->result();
    	return $result;
    }
    public function get_obat($id)
    {
    	$this->db->where('id',$id);
    	$query = $this->db->get('ruangan_farmasi_stock');
    	$result = $query->result();
    	return $result;
    }
    public function get_racikan($id)
    {
    	$this->db->where('id',$id);
    	$query = $this->db->get('farmasi_racikan');
    	$result = $query->result();
    	return $result;
    }
    public function get_perawat($id)
    {
    	$this->db->where('id',$id);
    	$query = $this->db->get('pegawai');
    	$result = $query->result();
    	return $result;
    }
    public function chats($id,$user_id)
    {
        $this->db->where_in('to_id',array($id,$user_id));
        $this->db->where_in('from_id',array($id,$user_id));
        $query = $this->db->get('view_chats');
        $result = $query->result();
    	return $result;
    }
    public function chat_send($data)
    {
        $sent = $this->db->insert('chat',$data);
        return (($sent) ? TRUE : FALSE);
    }
    public function general_chat()
    {
        $query = $this->db->get("general_chats");
        $result = $query->result();
        return $result;
    }
    public function general_chat_send($data)
    {
        $sent = $this->db->insert('chat_general',$data);
        return (($sent) ? TRUE : FALSE);
    }
    public function mod_search($string)
    {
        $ret = array();
        $this->db->like('title', $string)
                 ->where('publish',1);
        $query = $this->db->get('system_modules');
        $role = $this->ezrbac->getCurrentUser()->user_role_id;
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $title = unserialize($item->title);
                $access = unserialize($item->access);
                if(in_array($role,$access)):
                $data[$item->id] =array(
                    'title' => $title[$this->lang->lang()],
                    'icon' => $item->icon,
                    'quick_menu' => $this->quick_menu($item->id,$item->alias)            
                );
                endif;
            endforeach;
            return $data;
        endif;
        return $ret;
    }
    private function quick_menu($id,$uri)
    {
        $ret = array();
        $this->db->where('module_id',$id);
        $query = $this->db->get('menu');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $title = unserialize($item->title);
                $url = site_url($uri."/".$item->alias);
                $data[$item->id] =array(
                    'title' => $title[$this->lang->lang()],
                    'icon' => $item->icon,
                    'url' => $url            
                );
            endforeach;
            return $data;
        endif;
        return $ret;
    }
}

/* End of file ajax_model.php */
/* Location: ./site/models/ajax_model.php */