<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Options_Model extends CI_Model
{
    
    public function get_lookup($id='', $database, $where, $value = '', $recordID)
    {                                           
    	$value = (!empty($value) ? $value : 'title' );
        $this->db->select($recordID.','.$value.'');
        if($id!=""):
        $this->db->where($where,$id);
        endif;
        $query = $this->db->get($database);         
        if ($query->num_rows() > 0)
        { 
            $row = $query->result(); 
            if(!empty($row[0]))
            {     
            	$options = array('' => ' Pilih ');
                foreach ($row as $data)
                {   
                    $option = $data->{$recordID};
                    $options[$option] = ucwords($data->$value);
                }   
                
                return $options;   
            }
            else
            {
                return false;    
            }            
        } 
        return false;
    }
    
    public function wherin_lookup($id='', $database, $where, $value = '')
    {
    	$value = (!empty($value) ? $value : 'title' );
        $this->db->select('id,'.$value.'');
        if(!empty($id)):
        $this->db->where_in($where,$id);
        endif;
        $query = $this->db->get($database);
        if ($query->num_rows() > 0)
        {
            $row = $query->result();
            if(!empty($row[0]))
            {
            	$options = array('' => ' Pilih ');
                foreach ($row as $data)
                {
                    $option = $data->id;
                    $options[$option] = ucwords($data->$value);
                }

                return $options;
            }
            else
            {
                return false;
            }
        }
        return false;
    }

    public function lookup($id, $database, $filed, $value)
    {
        $this->db->select($value);
        $query = $this->db->get_where($database, array($filed => $id));
        if($query->num_rows() > 0):
            $row = $query->result();
            if(!empty($row[0])):
                $data = $row[0]->$value;
                return $data;
            else:
                return false;
            endif;
        endif;
        return false;
    }

 	public function lookup_opt($id='', $database, $where, $value = '')
    {                                           
    	$value = (!empty($value) ? $value : 'title' );
        $this->db->select('id,'.$value.'');
        if(!empty($id)):
        $this->db->where($where,$id);
   	 	$this->db->where('optional','1');
        endif;
        $query = $this->db->get($database);         
        if ($query->num_rows() > 0)
        { 
            $row = $query->result(); 
            if(!empty($row[0]))
            {     
            	$options = array('' => ' Pilih ');
                foreach ($row as $data)
                {   
                    $option = $data->id;
                    $options[$option] = $data->$value;
                }   
                
                return $options;   
            }
            else
            {
                return false;    
            }            
        } 
        return false;
    }

    public function multiple($id='', $database, $where, $value = 'title')
    {                                           
        $this->db->select('id,'.$value.'');
        if(!empty($id)):
        $this->db->where($where,$id);
        endif;
        $query = $this->db->get($database);         
        if ($query->num_rows() > 0)
        { 
            $row = $query->result(); 
            if(!empty($row[0]))
            {      
                foreach ($row as $data)
                {   
                    $option = $data->id;
                    $options[$option] = $data->$value;
                }   
                
                return $options;   
            }
            else
            {
                return false;    
            }            
        } 
        return false;
    }

    public function lookup_kamar($id='', $database, $where, $value = '', $kelas)
    {                                           
    	$value = (!empty($value) ? $value : 'title' );
        $this->db->select('id,'.$value.'');
        if(!empty($id)):
        $this->db->where($where,$id);
    	$this->db->where('kelas_id',$kelas);
        endif;
        $query = $this->db->get($database);         
        if ($query->num_rows() > 0)
        { 
            $row = $query->result(); 
            if(!empty($row[0]))
            {     
            	$options = array('' => ' Pilih ');
                foreach ($row as $data)
                {   
                    $option = $data->id;
                    $options[$option] = $data->$value;
                }   
                
                return $options;   
            }
            else
            {
                return false;    
            }            
        } 
        return false;
    }
}
