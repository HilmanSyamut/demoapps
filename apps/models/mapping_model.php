<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mapping_Model extends CI_Model
{
    public function get_loc()
    {
        $this->db->where('GroupCode',1);
        $query = $this->db->get("masterdata");
        $data = $query->result();
        return $data;
    }
    
    public function data_location($id)
    {                              
        $this->db->where('LocationID',$id);             
    	$query = $this->db->get("rak");
        $data = $query->result();
        $result = "";
        if(!empty($data)){
            foreach ($data as $rak) {
                $result[] = array(
                    'RackID' => $rak->RackID,
                    'GroupID' => $rak->RackID[1],
                    'RackName' => $rak->RackName,
                    'ColumnCount' => $rak->ColumnCount,
                    'RowCounts' => $rak->RowCounts,
                    'detail_rack' => $this->data_rack($rak->RackID)
                );
            }
        }
        return $result;
    }

    private function data_rack($id)
    {
        $this->db->where('ParentRackID',$id);             
        $query = $this->db->get("rak_detail");
        $data = $query->result();
        foreach ($data as $item) {
            $result[] = array(
                'RowNo' => $item->RowNo,
                'ColumnNo' => $item->ColumnNo,
                'ColumnID' => $item->ColumnID,
                'ColumnName' => $item->ColumnName,
                'MaxCapacity' => $item->MaxCapacity,
                'isi_rack' => $this->count_carton($item->ColumnID),
                "color" => $this->get_color($item->ColumnID),
                'default_color' => $this->def_color($item->Color)
            );
        }
        return $result;
    }
    
    private function def_color($id)
    {
        $this->db->where('NameCode',$id);             
        $query = $this->db->get("masterdata");
        $res = $query->first_row();
        return $res->GroupName;
    }

    private function count_carton($id)
    {
        $this->db->where('status',2);
        $this->db->where('location',$id);             
        $query = $this->db->get("carton");
        $data = $query->num_rows();
        return $data;
    }

    private function get_color($id)
    {
        $ship = 10000;
        $this->db->where('status',2);
        $this->db->where('location',$id);             
        $query = $this->db->get("carton");
        $data = $query->result();
        foreach ($data as $rak) {
            $ShipDate = count_week(now(),$rak->shipping_date);
            if($ship < $ShipDate ){ $ship = $ship; }else{ $ship = $ShipDate; }
        }
        if($ship <= 2){ $color = "bg-success";}elseif ($ship > 2 && $ship <= 4) {$color = "bg-warning";}elseif ($ship > 4 && $ship != 10000) {$color = "bg-danger";}elseif($ship == 10000){$color = "bg-primary";}
        return $color;
    }

    public function data_carton($id)
    {
        $this->db->order_by('ordering', "asc");
        $this->db->where('status',2);
        $this->db->where('location',$id);             
        $query = $this->db->get("carton");
        $data = $query->result();
        return $data;
    }

    public function data_rak($LocationID){
        $this->db->where('LocationID', $LocationID);
        $query = $this->db->get('rak');
        return $query;
    }

    public function data_masterdata(){
        $this->db->where('GroupCode', 1);
        $query = $this->db->get('masterdata');
        return $query;
    }

    public function data_ListRack($like){
        if (empty($like)) {

        }else{
        $this->db->like('location', $like);
        return $this->db->get('carton');
        }
    }

    public function data_ListRack_detail($dataID){
        $this->db->select('*');
        $this->db->from('carton');
        $this->db->join('carton_detail', 'carton.id=carton_detail.parent_id');
        $this->db->where('carton_detail.parent_id', $dataID);
        $query = $this->db->get();
        return $query;
    }

    public function getCartonByStatus($status){
        $this->db->from('carton');
        $this->db->where('status', $status);
        if($status == '2')
            $this->db->where('location', null);
        return $this->db->get();
    }
}
