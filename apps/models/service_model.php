<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service_Model extends CI_Model
{

	function __construct()
    {
    	parent::__construct();
    	$this->sytline = $this->load->database('sytline', TRUE);
	}

    public function insertDCMoveIN()
    {
        $data = array(
    		'trans_num' => 1,
    		'trans_date' => date("m-d-Y G:i:s",now()),
    		'emp_num' => 'GTA',
    		'trans_type' => 1,
    		'item' => "",
    		'whse' => "Warehouse",
    		'loc1' => "Packing",
    		'lot1' => null,
    		'loc2' => "Stock",
    		'lot2' => null,
    		'qty_moved' => 1,
    		'stat' => 'p',
    		'override' => 1,
    		'u_m ' => null,
    		'CanOverride' => null,
    		'remote_site_lot_process' => "B",
    		'create_loc' => 0,
    		'document_num' => "TO"
		);
		$this->db->trans_begin();
            $this->sytline->insert('dcmove',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }

    public function insertDCMoveRet()
    {
        $data = array(
    		'trans_num' => 1,
    		'trans_date' => date("d-m-Y G:i:s",now()),
    		'emp_num' => 'GTA',
    		'trans_type' => 1,
    		'item' => "",
    		'whse' => "Warehouse",
    		'loc1' => "Stock",
    		'lot1' => null,
    		'loc2' => "Packing",
    		'lot2' => null,
    		'qty_moved' => 1,
    		'stat' => 'p',
    		'override' => 1,
    		'u_m ' => null,
    		'CanOverride' => null,
    		'remote_site_lot_process' => "B",
    		'create_loc' => 0,
    		'document_num' => "TO"
		);
		$this->db->trans_begin();
            $this->sytline->insert('dcmove',$data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
}
