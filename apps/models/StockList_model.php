<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stocklist_model extends CI_Model
{    

    public function getList($limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField,$type){
        $this->db->where('t1020f004',$type);
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->select('*');
            $this->db->from('t1020 st');
            $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
            $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
            return $this->db->get();
        }else{
            if ($searchignoreCase == TRUE) {            
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->select('*');
                $this->db->from('t1020 st');
                $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
                $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
                $this->db->order_by($sortfield, $sortdir);
                return $this->db->get();
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->select('*');
                    $this->db->from('t1020 st');
                    $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
                    $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1020 st');
                    $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
                    $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1020 st');
                    $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
                    $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1020 st');
                    $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
                    $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1020 st');
                    $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
                    $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->select('*');
                    $this->db->from('t1020 st');
                    $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
                    $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }
            }
        }
    }

    //Count Data
    public function getListCount(){
        $this->db->select('*');
        $this->db->from('t1020 st');
        $this->db->join('t8010 item', 'item.t8010f001=st.t1020f001');
        $this->db->join('t8030 loc', 'loc.t8030f001=st.t1020f002');
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    // public function GetListDetail($itemid, $itemloc, $limit, $offset){
    public function getListDetail($pri, $itemloc, $limit, $offset, $sortfield, $sortdir, $searchignoreCase, $searchOperator, $searchValue, $searchField){
        if ($searchOperator == '' && $searchValue == '' && $searchField == '' && $searchignoreCase == '') {
            $this->db->select('*');
            $this->db->from('t1021 st');
            $this->db->where('st.t1021f001', $pri);
            // $this->db->where('st.t1021f005', $itemloc);
            $this->db->order_by($sortfield, $sortdir);
            $this->db->limit($limit, $offset);
            return $this->db->get();
        }else{
            if ($searchignoreCase == TRUE) {            
                $this->db->select('*');
                $this->db->from('t1021 st');
                $this->db->where('st.t1021f001', $pri);
                // $this->db->where('st.t1021f005', $itemloc);
                $this->db->where($searchField.' LIKE "%'.$searchValue.'%"');
                $this->db->order_by($sortfield, $sortdir);
                return $this->db->get();
            }else{
                if ($searchOperator == 'eq') {
                    $operator = '=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }
                    $this->db->select('*');
                    $this->db->from('t1021 st');
                    $this->db->where('st.t1021f001', $pri);
                    // $this->db->where('st.t1021f005', $itemloc);
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'neq'){
                    $operator = '!=';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1021 st');
                    $this->db->where('st.t1021f001', $pri);
                    // $this->db->where('st.t1021f005', $itemloc);
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'startswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1021 st');
                    $this->db->where('st.t1021f001', $pri);
                    // $this->db->where('st.t1021f005', $itemloc);
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'contains') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1021 st');
                    $this->db->where('st.t1021f001', $pri);
                    // $this->db->where('st.t1021f005', $itemloc);
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'doesnotcontain') {
                    $operator = 'NOT LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "%'.$searchValue.'%"');
                    }                    
                    $this->db->select('*');
                    $this->db->from('t1021 st');
                    $this->db->where('st.t1021f001', $pri);
                    // $this->db->where('st.t1021f005', $itemloc);
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }else if($searchOperator == 'endswith') {
                    $operator = 'LIKE';
                    if ($searchOperator != '' && $searchValue != '' && $searchField != '') {
                        $this->db->where($searchField.' '.$operator.' "'.$searchValue.'%"');
                    }
                    $this->db->select('*');
                    $this->db->from('t1021 st');
                    $this->db->where('st.t1021f001', $pri);
                    // $this->db->where('st.t1021f005', $itemloc);
                    $this->db->order_by($sortfield, $sortdir);
                    return $this->db->get();
                }
            }
        }
    }

    //Count Data
    public function getListDetailCount($pri, $itemloc){
        $this->db->select('*');
        $this->db->from('t1021 st');
        $this->db->where('t1021f001', $pri);
        // $this->db->where('t1021f005', $itemloc);
        $query = $this->db->get();
        $rowcount = $query->num_rows();
        return $rowcount;
    }

    public function stock_exist($itemid,$itemloc,$code){
        $this->db->from(T_ItemMovementStockListDetail1);
        $this->db->where(T_ItemMovementStockListDetail1_ItemID, $itemid);
        // $this->db->where(T_ItemMovementStockListDetail1_LocationID, $itemloc);
        if($code){ 
            $this->db->where(T_ItemMovementStockListDetail1_Code, $code); 
        }
        return $this->db->get();
    }

    public function item_exist($itemid,$itemloc,$type = ""){
        $this->db->from(T_TransactionStockBalanceHeader);
        $this->db->where(T_TransactionStockBalanceHeader_ItemID, $itemid);
        $this->db->where(T_TransactionStockBalanceHeader_LocationID, $itemloc);
        if($type!=""){ $this->db->where(T_ItemMovementStocklistHeader_Type, $type); }
        return $this->db->get();
    }

    public function InsertStockList($table,$data){
        $this->db->trans_begin();
        $this->db->insert($table, $data);
        $id = $this->db->insert_id();
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
            return FALSE;
        }
        else
        {
            $this->db->trans_commit();
            return $id;
        }
    }

    public function UpdateStockList($table,$data,$field,$id){
        $this->db->trans_begin();
        $this->db->where($field,$id);
        $this->db->update($table, $data);
        if ($this->db->trans_status() === FALSE)
        {
            $this->db->trans_rollback();
        }
        else
        {
            $this->db->trans_commit();
        }
    }
       
}
