<?php
class Profile_Model extends CI_Model
{

    public function __construct(){
        parent::__construct();
	}
    public function user_data($id)
    {
        $ret = array();
        $this->db->where('id',$id);
		$query= $this->db->get('system_users');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $data = array(
                    'id' => $item->id,
                    'username' => $item->email,
                    'password' => $item->password,
                    'salt' => $item->salt,
                    'user_role' => $item->user_role_id,
                    'last_login' => $item->last_login,
                    'last_login_ip' => $item->last_login_ip,
                    'verified' => $item->verification_status,
                    'status' => $item->status,
                    'meta' => (!empty($item->pegawai_id) ? $this->pegawai($item->pegawai_id) : $this->meta($item->id) ),
                    'activity' => $this->activity($item->id)
                );
            endforeach;
            return $data;
        endif;
		return $ret;
	}

    private function meta($id)
    {
        $ret = array();
        $this->db->where('user_id',$id);
        $query = $this->db->get('user_meta');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $photo = (!empty($item->photo) ? "assets/uploads/thumbs/".$item->photo." " : "assets/admin/layout/img/no-photo.png" );
                $data = array(
                    'first_name' => $item->first_name,
                    'last_name' => $item->last_name,
                    'full_name' => $item->first_name." ".$item->last_name,
                    'phone' => $item->phone,
                    'photo' => "<img src='".$photo."' class='img-responsive' alt='' />",
                    'address' => $item->address,
                    'provinsi' => $item->provinsi_id,
                    'kecamatan' => $item->kecamatan_id,
                    'kabkot' => $item->kabkot_id,
                    'facebook' => $item->facebook,
                    'twitter' => $item->twitter,
                    'website' => $item->website,
                    'office' => $item->office
                );
            endforeach;
            return $data;
        endif;
        return $ret;
    }
    private function activity($id)
    {
        $ret = array();
        $this->db->where('create_user',$id);
        $query = $this->db->get('activity_user');
        if($query->num_rows() > 0):
            foreach($query->result() as $item):
                $data[$item->id] = array(
                    'message' => $item->message,
                    'create_date' => '<abbr class="timeago" title="'.date("m d Y g:i:s",$item->create_datetime).'">'.date("d M Y g:i:s",$item->create_datetime).'</abbr>'
                );
            endforeach;
            return $data;
        endif;
        return $ret;
    }
}
