 <?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notification_Model extends CI_Model
{
    public function me_chats($id)
    {
        $result = array();
        $this->db->where('to_id',$id)
                 ->where('recd',0)
                 ->group_by('from');
        $query = $this->db->get('view_chats');
        $result['counts'] = $query->num_rows();
        $result['msg'] = $query->result();
        return $result;
    }
}
