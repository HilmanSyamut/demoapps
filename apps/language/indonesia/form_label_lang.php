<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Hospital Information System
 *
 * Application under BluesCode Framework
 * Compatible with PHP 5.4 or Lates
 *
 * @package	    BluesCode
 * @author	    Muhammad Arief
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------
/**
 * Form Label Lang
 *
 *
 * @package	    Site
 * @subpackage	Language
 * @category	Indonesia language
 * 
 * @version     1.1 Build 05.10.2014	
 * @author	    Muhammad Arief
 * @contributor 
 * @copyright	Copyright (c) 2013 - 2017, Global Trend Asia
 * @license	    http://www.cplus-studio.net/bluescode/license.html
 * @link	    http://www.muhammad-arief.com/bluescode.html | http://www.cplus-studio.net/bluescode.html
 */
// ------------------------------------------------------------------------

// form label
$lang['form_label_alias']				= 'Alias';
$lang['form_label_title']				= 'Judul';
$lang['form_label_description']			= 'Deskripsi';
$lang['form_label_icon']				= 'Icon';
$lang['form_label_addmenu']				= 'Form tambah menu baru';
$lang['form_label_type']				= 'Tipe';
$lang['form_label_ordering']			= 'Urutan';
$lang['form_label_location']			= 'Lokasi';
$lang['form_label_access']				= 'Akses';
$lang['register']				        = 'Pendaftaran';
$lang['my_profile']				        = 'Profil Saya';
$lang['logout']				            = 'Keluar';
$lang['emergency_patient']				= 'Pasien Rawat Darurat';
$lang['outpatient']				        = 'Pasien Rawat Jalan';
$lang['hospitalization_patient']		= 'Pasien Rawat Inap';
$lang['medical_support']		        = 'Penunjang Medis';
$lang['view_more']		                = 'Lihat Detail';
$lang['all_monthly_visit']		        = 'Kunjungan Pasien Bulanan';
$lang['underconstruction']		        = 'Dalam Tahap Pengerjaan';
$lang['doctor_schedule']		        = 'Jadwal Dokter';
$lang['label_date']		                = 'Tanggal';
$lang['label_norm']		                = 'No.RM';
$lang['label_name']		                = 'Name';
$lang['label_first_name']		        = 'Nama Depan';
$lang['label_last_name']		        = 'Nama Belakang';
$lang['label_identity']		            = 'Identitas';
$lang['label_identity_number']		    = 'Nomor Identitas';
$lang['label_gender']		            = 'Jenis Kelamin';
$lang['label_male']		                = 'Laki - Laki';
$lang['label_female']		            = 'Perempuan';
$lang['label_born']		                = 'Lahir';
$lang['label_birthcity']		        = 'Tempat Lahir';
$lang['label_birthdate']		        = 'Tanggal Lahir';
$lang['label_marital_status']		    = 'Status Perkawinan';
$lang['label_religion']		            = 'Agama';
$lang['label_employ']		            = 'Pekerjaan';
$lang['label_address']		            = 'Alamat';
$lang['label_province']		            = 'Provinsi';
$lang['label_district_city']            = 'Kabupaten/Kota';
$lang['label_subdistric']               = 'Kecamatan';
$lang['label_blood_type']               = 'Golongan Darah';
$lang['label_save']                     = 'Simpan';
$lang['label_cancel']                   = 'Batal';
$lang['label_new_patient']              = 'Pasien Baru';
$lang['label_old_patient']              = 'Pasien Lama';
$lang['label_patient_data']             = 'Data Pasien';
$lang['label_responsible_person']       = 'Penanggung Jawab';
$lang['label_visit']                    = 'Kunjungan';
$lang['label_php_error']                = 'Sebuah Kesalahan PHP ditemukan';
$lang['label_add_new']                  = 'Tambah Baru';
$lang['label_user_name']				= 'Nama Pengguna';