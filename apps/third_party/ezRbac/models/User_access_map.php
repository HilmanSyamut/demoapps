<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * user_access_map model class
 *
 * This model represents user access mapping data. It can be used
 * for manipulation and retriving access previlages information.
 *
 * @version	1.0
 * @package ezRbac
 * @since ezRbac v 0.2
 * @author Roni Kumar Saha<roni.cse@gmail.com>
 * @copyright Copyright &copy; 2012 Roni Saha
 * @license	GPL v3 - http://www.gnu.org/licenses/gpl-3.0.html
 *
 */
class User_access_map extends  CI_Model {
    /**
     * @var CI_Controller CI instance reference holder
     */
    private $CI;

    /**
     * @var String $_table_name store access_map_table name
     */
    private $_table_name;

    /**
     * @var String $_user_role_table store user_role_table name
     */
    private $_user_role_table;


    /**
     * Constructor Function
     */
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->CI=& get_instance();

        $this->_table_name=$this->CI->config->item('access_map_table','ez_rbac');
        $this->_user_role_table=$this->CI->config->item('user_role_table','ez_rbac');
    }

    /**
     * Get permission info by access_role and controller
     *
     * @param string $access_role
     * @param string $controller
     * @return    object
     */
    function get_permission($access_role,$controller="")
    {
        //Default return value
        $ret=array();
        $this->db->select('t0050f003,t0050f002');
        $this->db->where('t0050f001',$access_role);
        if($controller!=""){
            $this->db->where('t0050f002',$controller);
            $ret =NULL;
        }

        $query = $this->db->get($this->_table_name);
        if ($query->num_rows() > 0) {
            if($controller==""){    //Get all permission info for selected user
                $output=array();
                foreach ($query->result() as $prow) {
                    $output[$prow->t0050f002]=$prow->t0050f002;
                }
                return $output;
            }
            $row=$query->row();
            return $row->t0050f003;
        }
        return $ret;
    }

    /**
     * Get permission info by access_role and controller
     *
     * @param string $access_role
     * @param string $controller
     * @return    object
     */
    function get_method($access_role,$controller="")
    {
        //Default return value
        $ret=array();
        $this->db->select('method,controller');
        $this->db->where('user_role_id',$access_role);
        if($controller!=""){
            $this->db->where('controller',$controller);
            $ret =NULL;
        }

        $query = $this->db->get($this->_table_name);
        if ($query->num_rows() > 0) {
            if($controller==""){    //Get all permission info for selected user
                $output=array();
                foreach ($query->result() as $prow) {
                    $output[$prow->controller]=$prow->method;
                }
                return $output;
            }
            $row=$query->row();
            return $row->method;
        }
        return $ret;
    }

    /**
     * Save access permission to database
     * @param $controller
     * @param $role
     * @param $permission
     * @return mixed
     */
    function set_permission($controller,$role,$permission){
        $data['user_role_id']=$role;
        $data['controller']=$controller;
        $data['permission']=$permission;
        $where=array('user_role_id'=>$role,'controller'=>$controller);
        $query = $this->db->get_where($this->_table_name, $where, 1, 0);
        if ($query->num_rows() == 0) { //Insert
            $this->db->insert($this->_table_name, $data);
            return;
        }
        //Existing data so update
        $this->db->update($this->_table_name, $data,$where);
    }

    /**
     * Get permission info by access_role and controller
     *
     * @param string $access_role
     * @param string $controller
     * @return    object
     */
    function checking_module($module)
    {
        //Default return value
        $ret=array();
        $this->db->where('alias',$module);
        $query = $this->db->get('system_modules');
        if ($query->num_rows() > 0) {
            $row=$query->row();
            return $row->alias;
        }
        return $ret;
    }

    /**
     * Get permission info by access_role and controller
     *
     * @param string $access_role
     * @param string $controller
     * @return    object
     */
    function checking_controller($module,$controller)
    {
        //Default return value
        $ret=array();
        $this->db->where('module',$module);
        $this->db->where('controller',$controller);
        $query = $this->db->get('view_controller');
        if ($query->num_rows() > 0) {
            $row=$query->row();
            return $row->controller;
        }
        return $ret;
    }

    /**
     * Get permission info by access_role and controller
     *
     * @param string $access_role
     * @param string $controller
     * @return    object
     */
    function checking_roles($module,$controller,$method)
    {
        //Default return value
        $id = $this->method_id($module,$controller,$method);
        $user_access = $this->user_access();
        $role_id = $this->CI->session->userdata('access_role');
        $ret=array();
        $this->db->where('t0080r001',$role_id);
        $query = $this->db->get('t0080');
        if ($query->num_rows() > 0) {
            $row=$query->row();
            $user_access = unserialize($user_access);
            $access = unserialize($row->t0080f004);
            $access = array_merge($user_access, $access);
            $access = array_unique($access);
            $access = serialize($access);
            $result = array('id' => $id, 'access' => $access);
            return $result;
        }
        return $ret;
    }

    private function method_id($module,$controller,$method)
    {
        //Default return value
        $ret=array();
        if(!empty($module)):
            $this->db->where('module',$module);
        endif;
        $this->db->where('controller',$controller);
        $this->db->where('method',$method);
        $query = $this->db->get('v0020');
        if ($query->num_rows() > 0) {
            $row=$query->row();
            return $row->id;
        }
        return $ret;
    }

    private function user_access()
    {
        $ret=array();
        $user_id = $this->CI->session->userdata('user_id');
        if(!empty($user_id)):
            $this->db->where('t0040r001',$user_id);
        endif;
        $query = $this->db->get('t0040');
        if ($query->num_rows() > 0) {
            $row=$query->row();
            return $row->t0040f017;
        }
        return $ret;
    }
}


/* End of file user_access_map.php */
/* Location: ./ezRbac/models/user_access_map.php */
