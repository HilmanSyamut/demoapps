<html>
    <head>
        <title>
            Developer Tools - Print Out :: BluesCode Framework
        </title>
        <script type="text/javascript">
            String.prototype.trim = function()
            {
                return this.replace(/^\s+|\s+$/g,"");
            }
            function codeToList()
            {
                //Convert code elements to ordered lists
                var code = document.getElementsByTagName("code")
                for (i = 0; i < code.length; i++)
                {
                    code[i].innerHTML = "<ol class='code'>" + code[i].innerHTML.replace(/\n/g, "<li>").trim() + "</ol>";
                } //end for
                //Apply zebra stripes
                if (document.getElementsByClassName)
                {
                    var code = document.getElementsByClassName("code");
                    for (i = 0; i < code.length; i++)
                    {	
                        var li = code[i].getElementsByTagName("li")
                        var cn = "odd";						
                        for (x = 0; x < li.length; x++)
                        {
                            li[x].className = cn;				
                            cn == "odd" ? cn = "even" : cn = "odd"; //condition ? true : false
                        } //end for
                    } //end for
                } //end if
            } //end function
            window.onload = DOMReadyAll;
            function DOMReadyAll()
            {
                codeToList();
            }
            function selectCode(id) {
                if (document.selection) {
                    var div = document.body.createTextRange();
                    div.moveToElementText(document.getElementById(id));
                    div.select();
                }
                else {
                    var div = document.createRange();
                    div.setStartBefore(document.getElementById(id));
                    div.setEndAfter(document.getElementById(id));
                    window.getSelection().addRange(div);
                }
            }
            //gogo global variable
            var level=0;
            var LOOP_SIZE=100;
            function runTabifier() {
                var code=document.getElementById('code').innerHTML;
                var type=document.getElementById('code_type');
                type=type.options[type.selectedIndex].value;
                if ('HTML'==type) cleanHTML(code);
                if ('CSS'==type) code=cleanCSS(code);
                if ('cstyle'==type) code=cleanCStyle(code);
                if ('JSON'==type) code=cleanJson(code);
                code=code.replace(/\n\s*\n/g, '\n');  //blank lines
                code=code.replace(/^[\s\n]*/, ''); //leading space
                code=code.replace(/[\s\n]*$/, ''); //trailing space
                document.getElementById('code').innerHTML = code;                
            }
            function cleanHTML(code) {
                var i=0;
                function cleanAsync() {
                    var iStart=i;
                    for (; i<code.length && i<iStart+LOOP_SIZE; i++) {
                        point=i;
                        //if no more tags, copy and exit
                        if (-1==code.substr(i).indexOf('<')) {
                            out+=code.substr(i);
                            finishTabifier(out);
                            return;
                        }
                        //copy verbatim until a tag
                        while (point<code.length && '<'!=code.charAt(point)) point++;
                        if (i!=point) {
                            cont=code.substr(i, point-i);
                            if (!cont.match(/^\s+$/)) {
                                if ('\n'==out.charAt(out.length-1)) {
                                    out+=tabs();
                                } else if ('\n'==cont.charAt(0)) {
                                    out+='\n'+tabs();
                                    cont=cont.replace(/^\s+/, '');
                                }
                                cont=cont.replace(/\s+/g, ' ');
                                out+=cont;
                            } if (cont.match(/\n/)) {
                                out+='\n'+tabs();
                            }
                        }
                        start=point;
                        //find the end of the tag
                        while (point<code.length && '>'!=code.charAt(point)) point++;
                        tag=code.substr(start, point-start);
                        i=point;
                        //if this is a special tag, deal with it!
                        if ('!--'==tag.substr(1,3)) {
                            if (!tag.match(/--$/)) {
                                while ('-->'!=code.substr(point, 3)) point++;
                                point+=2;
                                tag=code.substr(start, point-start);
                                i=point;
                            }
                            if ('\n'!=out.charAt(out.length-1)) out+='\n';
                            out+=tabs();
                            out+=tag+'>\n';
                        } else if ('!'==tag[1]) {
                            out=placeTag(tag+'>', out);
                        } else if ('?'==tag[1]) {
                            out+=tag+'>\n';
                        } else if (t=tag.match(/^<(script|style)/i)) {
                            t[1]=t[1].toLowerCase();
                            tag=cleanTag(tag);
                            out=placeTag(tag, out);
                            end=String(code.substr(i+1)).toLowerCase().indexOf('</'+t[1]);
                            if (end) {
                                cont=code.substr(i+1, end);
                                i+=end;
                                out+=cont;
                            }
                        } else {
                            tag=cleanTag(tag);
                            out=placeTag(tag, out);
                        }
                    }
                }
                var point=0, start=null, end=null, tag='', out='', cont='';
                cleanAsync();
            }
            function tabs() {
                var s='';
                for (var j=0; j<level; j++) s+='\t';
                return s;
            }
            function cleanTag(tag) {
                var tagout='';
                tag=tag.replace(/\n/g, ' ');       //remove newlines
                tag=tag.replace(/[\s]{2,}/g, ' '); //collapse whitespace
                tag=tag.replace(/^\s+|\s+$/g, ' '); //collapse whitespace
                var suffix='';
                if (tag.match(/\/$/)) {
                    suffix='/';
                    tag=tag.replace(/\/+$/, '');
                }
                tag=tag.replace(/^\s+|\s+$/g, ' '); //collapse whitespace
                tag=tag.split(' ');
                for (var j=0; j<tag.length; j++) {
                    if (-1==tag[j].indexOf('=')) {
                        //if this part doesn't have an equal sign, just lowercase it and copy it
                        tagout+=tag[j].toLowerCase()+' ';
                    } else {
                        //otherwise lowercase the left part and...
                        var k=tag[j].indexOf('=');
                        var tmp=[tag[j].substr(0, k), tag[j].substr(k+1)];
                        tagout+=tmp[0].toLowerCase()+'=';
                        var x=tmp[1].charAt(0);
                        if ("'"==x || '"'==x) {
                            //if the right part starts with a quote, find the rest of its parts
                            tagout+=tmp[1];
                            while(x!=String(tag[j]).charAt(String(tag[j]).length-1)) {
                                tagout+=' '+tag[++j];
                            }
                            tagout+=' ';
                        } else {
                            //otherwise put quotes around it
                            tagout+="'"+tmp[1]+"' ";
                        }
                    }
                }
                return tagout.replace(/\s*$/, '')+suffix+'>';
            }
            /////////////// The below variables are only used in the placeTag() function
            /////////////// but are declared global so that they are read only once
            //opening and closing tag on it's own line but no new indentation level
            var ownLine=['area', 'body', 'head', 'hr', 'i?frame', 'link', 'meta',
                'noscript', 'style', 'table', 'tbody', 'thead', 'tfoot'];
            //opening tag, contents, and closing tag get their own line
            //(i.e. line before opening, after closing)
            var contOwnLine=['li', 'dt', 'dt', 'h[1-6]', 'option', 'script'];
            //line will go before these tags
            var lineBefore=new RegExp(
            '^<(/?'+ownLine.join('|/?')+'|'+contOwnLine.join('|')+')[ >]'
        );
            //line will go after these tags
            lineAfter=new RegExp(
            '^<(br|/?'+ownLine.join('|/?')+'|/'+contOwnLine.join('|/')+')[ >]'
        );
            //inside these tags (close tag expected) a new indentation level is created
            var newLevel=['blockquote', 'div', 'dl', 'fieldset', 'form', 'frameset',
                'map', 'ol', 'p', 'pre', 'select', 'td', 'th', 'tr', 'ul'];
            newLevel=new RegExp('^</?('+newLevel.join('|')+')[ >]');
            function placeTag(tag, out) {
                var nl=tag.match(newLevel);
                if (tag.match(lineBefore) || nl) {
                    out=out.replace(/\s*$/, '');
                    out+="\n";
                }
                if (nl && '/'==tag.charAt(1)) level--;
                if ('\n'==out.charAt(out.length-1)) out+=tabs();
                if (nl && '/'!=tag.charAt(1)) level++;
                out+=tag;
                if (tag.match(lineAfter) || tag.match(newLevel)) {
                    out=out.replace(/ *$/, '');
                    out+="\n";
                }
                return out;
            }
            function cleanCSS(code) {
                var i=0, instring=false, c;
                function cleanAsync() {
                    var iStart=i;
                    for (; i<code.length && i<iStart+LOOP_SIZE; i++) {
                        c=code.charAt(i);
                        if (instring) {
                            if (instring==c) {
                                instring=false;
                            }
                            out+=c;
                        } else if ('{'==c) {
                            level++;
                            out+=' {\n'+tabs();
                        } else if ('}'==c) {
                            out=out.replace(/\s*$/, '');
                            level--;
                            out+='\n'+tabs()+'}\n'+tabs();
                        } else if ('"'==c || "'"==c) {
                            if (instring && c==instring) {
                                instring=false;
                            } else {
                                instring=c;
                            }
                            out+=c;
                        } else if (';'==c) {
                            out+=';\n'+tabs();
                        } else if ('\n'==c) {
                            out+='\n'+tabs();
                        } else {
                            out+=c;
                        }
                    }
                }
                if ('\n'==code[0]) code=code.substr(1);
                code=code.replace(/([^\/])?\n*/g, '$1');
                code=code.replace(/\n\s+/g, '\n');
                code=code.replace(/[	 ]+/g, ' ');
                code=code.replace(/\s?([;:{},+>])\s?/g, '$1');
                code=code.replace(/\{(.*):(.*)\}/g, '{$1: $2}');
                var out=tabs(), li=level;
                cleanAsync();
            }
            function cleanCStyle(code) {
                var i=0;
                function cleanAsync() {
                    var iStart=i;
                    for (; i<code.length && i<iStart+LOOP_SIZE; i++) {
                        c=code.charAt(i);
                        if (incomment) {
                            if ('//'==incomment && '\n'==c) {
                                incomment=false;
                            } else if ('/*'==incomment && '*/'==code.substr(i, 2)) {
                                incomment=false;
                                c='*/\n';
                                i++;
                            }
                            if (!incomment) {
                                while (code.charAt(++i).match(/\s/)) ;; i--;
                                c+=tabs();
                            }
                            out+=c;
                        } else if (instring) {
                            if (instring==c && // this string closes at the next matching quote
                            // unless it was escaped, or the escape is escaped
                            ('\\'!=code.charAt(i-1) || '\\'==code.charAt(i-2))
                        ) {
                                instring=false;
                            }
                            out+=c;
                        } else if (infor && '('==c) {
                            infor++;
                            out+=c;
                        } else if (infor && ')'==c) {
                            infor--;
                            out+=c;
                        } else if ('else'==code.substr(i, 4)) {
                            out=out.replace(/\s*$/, '')+' e';
                        } else if (code.substr(i).match(/^for\s*\(/)) {
                            infor=1;
                            out+='for (';
                            while ('('!=code.charAt(++i)) ;;
                        } else if ('//'==code.substr(i, 2)) {
                            incomment='//';
                            out+='//';
                            i++;
                        } else if ('/*'==code.substr(i, 2)) {
                            incomment='/*';
                            out+='\n'+tabs()+'/*';
                            i++;
                        } else if ('"'==c || "'"==c) {
                            if (instring && c==instring) {
                                instring=false;
                            } else {
                                instring=c;
                            }
                            out+=c;
                        } else if ('{'==c) {
                            level++;
                            out=out.replace(/\s*$/, '')+' {\n'+tabs();
                            while (code.charAt(++i).match(/\s/)) ;; i--;
                        } else if ('}'==c) {
                            out=out.replace(/\s*$/, '');
                            level--;
                            out+='\n'+tabs()+'}\n'+tabs();
                            while (code.charAt(++i).match(/\s/)) ;; i--;
                        } else if (';'==c && !infor) {
                            out+=';\n'+tabs();
                            while (code.charAt(++i).match(/\s/)) ;; i--;
                        } else if ('\n'==c) {
                            out+='\n'+tabs();
                        } else {
                            out+=c;
                        }
                    }
                }
                code=code.replace(/^[\s\n]*/, ''); //leading space
                code=code.replace(/[\s\n]*$/, ''); //trailing space
                code=code.replace(/[\n\r]+/g, '\n'); //collapse newlines
                var out=tabs(), li=level, c='';
                var infor=false, forcount=0, instring=false, incomment=false;
                cleanAsync();
            }
            function cleanJson(code) {
                var i=0;
                function cleanAsync() {
                    var iStart=i;
                    for (; i<code.length && i<iStart+LOOP_SIZE; i++) {
                        c=code.charAt(i);
                        if (instring) {
                            if (instring==c && // this string closes at the next matching quote
                            // unless it was escaped, or the escape is escaped
                            ('\\'!=code.charAt(i-1) || '\\'==code.charAt(i-2))
                        ) {
                                instring=false;
                            }
                            out+=c;
                        } else if ('"'==c || "'"==c) {
                            if (instring && c==instring) {
                                instring=false;
                            } else {
                                instring=c;
                            }
                            out+=c;
                        } else if ('{'==c || '['==c) {
                            level++;
                            out+=c+'\n'+tabs();
                            while (code.charAt(++i).match(/\s/)) ;; i--;
                        } else if ('}'==c || ']'==c) {
                            out=out.replace(/\s*$/, '');
                            level--;
                            if (!out.match(/({|\[)$/)) out+='\n'+tabs();
                            out+=c;
                            while (code.charAt(++i).match(/\s/)) ;; i--;
                        } else if (','==c) {
                            out+=',\n'+tabs();
                            while (code.charAt(++i).match(/\s/)) ;; i--;
                        } else {
                            out+=c;
                        }
                    }
                }
                code=code.replace(/^[\s\n]*/, ''); //leading space
                code=code.replace(/[\s\n]*$/, ''); //trailing space
                code=code.replace(/[\n\r]+/g, '\n'); //collapse newlines
                var out=tabs(), li=level, c='';
                var infor=false, forcount=0, instring=false, incomment=false;
                cleanAsync();
            }        
        </script>
        <style type="text/css">
            body 
            { 
                font-family: "Consolas", "Courier New", Courier, mono; 
                font-size: 11px;
                margin:25px; 
            }
            h1
            {
                color:#790000;
                padding:0px;
                margin:0px 0px 10px 0px;
            }
            .code
            {
                background-color: #ebebeb;
                border: 1px solid #cccccc;
                list-style-type: none;
                margin: 0px;
                padding: 0px;
                width: 100% !important;
                max-height:400px;
                white-space: pre-wrap;                 /* CSS3 browsers  */
                white-space: -moz-pre-wrap !important; /* 1999+ Mozilla  */
                white-space: -pre-wrap;                /* Opera 4 thru 6 */
                white-space: -o-pre-wrap;              /* Opera 7 and up */
                word-wrap: break-word;                 /* IE 5.5+ and up */         
                overflow:auto;
            }
            .code li
            {
                color: #5c8f29;
                font-family: "Consolas", "Courier New", Courier, mono;
                line-height: 0px; /* to remove white border issue */
                white-space: pre;            
            }
            .code { counter-reset: li; }
            .code li:before
            {
                counter-increment: li;
                content: counter(li) ". ";
                background: #ececec;
                border-right: 1px solid #cccccc;
                color: #555555;
                display: inline-block;
                line-height: 24px; /* minumum line height = 24, smaller causes white border issue */
                margin-right: 5px;
                padding-left:5px;
                padding-right: 5px;
                text-align: right;
                width: 40px;
            }
            .code li:nth-child(odd) { background: #ffffff; }
            .code li:nth-child(even) { background: #fafafa; }
            .code .odd { background: #ffffff; }
            .code .even { background: #fafafa; }
            .code li:empty {  }
            .copyright 
            { 
                padding-top:10px;
                color:#790000; 
            }
            table {
                width:auto;
            }
            table td 
            {
                padding:5px;
                border-bottom:dotted 1px #F8C301;
            }
            table th 
            {
                font-weight:bold;
                text-align:left;
                border-bottom:dotted 1px #F8C301;
                padding-right:15px;
            }
            table th ul 
            {
                padding-left:10px;
                margin:0px;
            }
            table th li 
            {
                padding-left:10px;
                list-style-type:none;
            }
            ol.tracer
            {
                padding:5px 0px 5px 0px;
                margin:0px 0px 0px 20px;
            }
            ol.tracer li
            {
                padding:0px 0px 5px 0px;    
            }
            ol.tracer li span
            {
                color:#666;
            }
            button
            {
                -webkit-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
                -moz-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
                box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
                border-bottom-color:#333;
                border:1px solid #5c8f29;
                background-color:#5c8f29;
                border-radius:5px;
                -moz-border-radius:5px;
                -webkit-border-radius:5px;
                color:#fff;
                font-size:9px;
                text-transform: uppercase;
                padding:2px 5px 2px 5px;
                margin:10px 0px 10px 0px;
            }
            select
            {
                -webkit-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
                -moz-box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
                box-shadow:rgba(0,0,0,0.2) 0 1px 0 0;
                border-bottom-color:#333;
                border:1px solid #5c8f29;
                background-color:#5c8f29;
                border-radius:5px;
                -moz-border-radius:5px;
                -webkit-border-radius:5px;
                color:#fff;
                font-size:9px;
                text-transform: uppercase;
                padding:2px 5px 2px 5px;
                margin:10px 0px 10px 0px;
            }            
        </style>
    </head>
    <body>
        <h1>Developer Tools - Print Out</h1>
        <button onclick="javacript:selectCode('code')" style="float:right;">Select Code</button>
        <select id="code_type">
            <option value="HTML">HTML</option>
            <option value="CSS">CSS</option>
            <option value="JSON">JSON</option>
        </select>
        <button onclick="javacript:runTabifier()">Format Code</button>
        <code id="code">
            <?php echo $data; ?>
        </code>
        <button onclick="javacript:selectCode('code')" style="float:right;">Select Code</button>            
        <div class="copyright">Copyright &copy <?php echo date('Y'); ?> muhammad arief :: BluesCode Framework, HTML and CSS 2.0 Compatible</div>        
    </body>
</html>