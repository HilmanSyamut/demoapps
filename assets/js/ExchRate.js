function ChangeCurrency(target,type,exch) {
    var result = 0;
    var Qty = 0;
    var UP = 0;
    var LN = 0;
    var BM = 0;
    if(exch){
        var ExchRate = exch;
    }else{
        var ExchRate = $("#ExchRate").data("kendoNumericTextBox").value();
    }
    var indexQty = $("[data-col=Qty]").index();
    var indexUP = $("[data-col=UnitPrice]").index();
    var indexBM = $("[data-col=BeaMasuk]").index();
    var indexLN = $("[data-col=LineTotal]").index();
    var rowCount = document.getElementById("list-" + target).rows.length - 1;
    for (var i = 0; i <= rowCount; i++) {
        var itemcel = $('#list-' + target + ' tr').eq(i);
        
        Qty = itemcel.find('td').eq(indexQty).attr("data-val");
        UP = itemcel.find('td').eq(indexUP).attr("data-def");
        //UP = accounting.unformat(UP);
        var lb = CurrencyLabel();
        if(type=="USD"){
          var newUP = parseInt(UP) / parseInt(ExchRate);
        }else if(type=="IDR"){
          var newUP = UP;
        }else if(type=="YEN"){
          var newUP = parseInt(UP) / parseInt(ExchRate);
        }
        UP = Math.round(newUP * 100) / 100;
        vUP = accounting.formatMoney(UP,lb,2,",");
        LN = Qty * UP;
        LN = Math.round(LN * 100) / 100;
        vLN = accounting.formatMoney(LN,lb,2,",");
        if(indexBM >0){
            BM = LN * 5/100;
            BM = Math.round(BM * 100) / 100;
            vBM = accounting.formatMoney(BM,lb,2,",");
            itemcel.find('td').eq(indexBM).attr("data-val",BM);
            itemcel.find('td').eq(indexBM).html(vBM);
        }
        if(indexUP > 0){
            itemcel.find('td').eq(indexUP).attr("data-val",UP);
            itemcel.find('td').eq(indexUP).html(vUP);
        }
        if(indexLN > 0){
            itemcel.find('td').eq(indexLN).attr("data-val",LN);
            itemcel.find('td').eq(indexLN).html(vLN);
        }
    }
    sumTotal(target);
    return result;
}

function ExchRateChange(id)
{
    var Currency = $("#CurrencyID").val();
    ChangeCurrency("detail",Currency,id);
}

function ExchRateListChange(){
    var Currency = $("#CurrencyID").val();
    var KursType = $("#ExchRateList").val();
    var Kurs = $("#Kurs").val();
    if(Currency!="IDR"){
        var ExchRate = $("#ExchRate").data("kendoNumericTextBox");
        ExchRate.value(0);
        ExchRate.readonly(false);
        $(".Kurs").removeClass("hide");
    }else{
        $("#ExchRateList").data("kendoDropDownList").value(1);
        var ExchRate = $("#ExchRate").data("kendoNumericTextBox");
        ExchRate.value(1);
        ExchRate.readonly();
        $(".Kurs").addClass("hide");
    }
    var voData = {
        table: true,
    };
    $.ajax({
        type: 'GET',
        data: voData,
        url:  site_url('Webservice/Read/GetExchangeRate/'+Currency+'/'+KursType),
        success: function (result) {
            if(result.errorcode == 0)
            {
                $("#ExchRate").data("kendoNumericTextBox").value(result.data[Kurs]);
                ChangeCurrency("detail",Currency);
            }else{
                if(Currency  =="IDR"){
                    ChangeCurrency("detail",Currency);
                }
            }
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(jQuery.parseJSON(jqXHR.responseText));
        }
    });
}